<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Code extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['module', 'slug', 'module_type', 'prefix', 'start', 'last_used'];

    const VISAS = 'Visas';
    const GROUP_TOURS = 'Group Tours';
    const PACKAGE_TOURS = 'Package Tours';
    const DAY_TOURS = 'Activities';
    const TAILORMADE_TOURS = 'Tailormade Tours';

    private static function getCode ($data) {
        return self::where('slug', str_slug($data['module']))
                    ->where('module_type', $data['module_type'])
                    ->first();
    }

    public static function getVisaEnquiryCode () {
        $data = ['module' => self::VISAS, 'module_type' => 'enquiry'];
        return self::getCode($data);
    }

    public static function getVisaBookingCode () {
        $data = ['module' => self::VISAS, 'module_type' => 'booking'];
        return self::getCode($data);
    }

    public function getNextCode () {
        $this->updateLastUsed(); // Increment anyway.
        return $this->prefix . $this->last_used;
    }

    public function updateLastUsed () {
        $this->last_used = $this->last_used + 1;
        $this->save();
    }
}
