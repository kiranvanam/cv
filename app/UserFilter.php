<?php

namespace App;

use App\Constants\UserGroup;

trait UserFilter {
    public static function adminFilter ()
    {
        return User::whereIn('usergroup', [
            UserGroup::SUPERUSER,
            UserGroup::SUPERADMIN,
            UserGroup::ADMIN,
            UserGroup::STAFF
        ]);
    }

    public static function customerFilter ()
    {
        return User::whereIn('usergroup', [
            UserGroup::REGISTEREDCUSTOMER,
            UserGroup::GUESTCUSTOMER,
            UserGroup::SUBSCRIBER
        ]);
    }
}
