<?php

namespace App;

use App\UserFilter;
use Admin\Auth\Models\Role;
use Admin\Enquiries\Models\VisaEnquiry;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use UserFilter;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'is_active', 'usergroup', 'country_id', 'calling_code', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function visaEnquiries()
    {
        return $this->hasMany(VisaEnquiry::class, 'customer_id');
    }

    public function createdVisaEnquiries()
    {
        return $this->hasMany(VisaEnquiry::class, 'created_by');
    }

    public function assignedVisaEnquiries()
    {
        return $this->hasMany(VisaEnquiry::class, 'assigned_to');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function permissions ()
    {
        $user = static::with('roles')->find($this->id);
        $permissionIds = [];
        $user->roles->every(function ($role) use (&$permissionIds) {
            $permissionIds = array_merge($permissionIds,
                                $role->permissions
                                ->pluck('id')
                                ->toArray()
                            );
        });

        $resources = Resource::with(['permissions' => function ($query) use ($permissionIds) {
                                        $query->whereIn('id', $permissionIds);
                                    }])
                                    ->get();
        return $resources;
    }

    public function hasPermission ($permission)
    {
        if(empty($this->getResources()))
            $this->initialize($this->permissions());

        if($this->permissionSearch($permission) == false) {
            throw new AuthorizationException("You are not authorized to perform this action");
        }

        return true;
    }

    // Return true if the user is super user
    public function isSuperUser () {
        if(strcmp($this->usergroup, "superuser") === 0) {
            return true;
        }
        return false;
    }

    // Return true if the user is not a customer or subscriber
    public function isNotCustomerOrSubscriber () {
        return !$this->isCustomerOrSubscriber();
    }

    // Return true if the user is super user
    public function isSuperAdmin () {
        if(strcmp($this->usergroup, "superadmin") === 0) {
            return true;
        }
        return false;
    }

    // Return Users who are Travel Agency's Staff or Owner
    public function isAdmin () {
        $admins = ['superadmin', 'admin'];
        if (in_array($this->usergroup, $admins)) {
            return true;
        }
        return false;
    }

    public static function fetchAdmins () {
        $admins = ['superuser', 'superadmin', 'admin'];
        return User::whereIn('usergroup', $admins)->get();
    }

    // Return Users who are registered customers or guest customers or subscribers
    public function isCustomerOrSubscriber () {
        $customers = ['registeredcustomer', 'guestcustomer', 'subscriber'];
        if (in_array($this->usergroup, $admins)) {
            return true;
        }
        return false;
    }

    // Return Users who are registered customers or guest customers
    public function isCustomer () {
        // Return true if the user is either registered user or guest customer
        $customers = ['registeredcustomer', 'guestcustomer'];
        if (in_array($this->usergroup, $admins)) {
            return true;
        }
        return false;
    }
}
