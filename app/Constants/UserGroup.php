<?php
namespace App\Constants;

class UserGroup
{
    use HasEnums;

    const SUPERUSER = 'superuser';
    const SUPERADMIN = 'superadmin';
    const ADMIN = 'admin';
    const STAFF = 'staff';
    const GUESTCUSTOMER = 'guestcustomer';
    const REGISTEREDCUSTOMER = 'registeredcustomer';
    const SUBSCRIBER = 'subscriber';
}
