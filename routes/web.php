<?php

use Admin\Locations\Models\Country;
use Illuminate\Support\Facades\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('admin.home');
    });
    Route::get('{any}', function () {
        return view('admin.home');
    })->where('any', '.*');
});

Auth::routes();

Route::get('logout', 'HomeController@logout');

Route::get('countries', function () {
    return Country::all();
});

Route::get('test', function () {
    // return \Auth::user();
    return phpinfo();
});
