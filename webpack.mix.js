const { mix } = require('laravel-mix');
var node_modules = __dirname + '/node_modules/';
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
*/
/*
 |--------------------------------------------------------------------------
 | Compile The Admin SPA
 |--------------------------------------------------------------------------
 */
mix.js('resources/assets/js/admin/main.js', 'public/js/admin')
    .extract(['vue']);

// mix.copyDirectory('resources/assets/admin/', 'public/cvadmin/');

mix.copyDirectory(node_modules + 'tinymce/', 'public/js/admin/tinymce/');
/*
mix.browserSync({
    proxy: 'cv.dev'
});
*/
