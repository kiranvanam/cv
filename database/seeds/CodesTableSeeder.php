<?php

use App\Code;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class CodesTableSeeder extends Seeder
{
    protected $services;
    public function __construct () {
        $this->services = include('Services.php');
    }

    public function run() {
        $codes = [
            [
                'module' => $this->services['visas'],
                'slug' => str_slug($this->services['visas']),
                'module_type' => 'enquiry',
                'prefix' => 'CVVE',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['groupTours'],
                'slug' => str_slug($this->services['groupTours']),
                'module_type' => 'enquiry',
                'prefix' => 'CVGTE',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['packageTours'],
                'slug' => str_slug($this->services['packageTours']),
                'module_type' => 'enquiry',
                'prefix' => 'CVPTE',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['activities'],
                'slug' => str_slug($this->services['activities']),
                'module_type' => 'enquiry',
                'prefix' => 'CVACTE',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['tailormadeTours'],
                'slug' => str_slug($this->services['tailormadeTours']),
                'module_type' => 'enquiry',
                'prefix' => 'CVTME',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['visas'],
                'slug' => str_slug($this->services['visas']),
                'module_type' => 'booking',
                'prefix' => 'CVVB',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['groupTours'],
                'slug' => str_slug($this->services['groupTours']),
                'module_type' => 'booking',
                'prefix' => 'CVGTB',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['packageTours'],
                'slug' => str_slug($this->services['packageTours']),
                'module_type' => 'booking',
                'prefix' => 'CVPTB',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['activities'],
                'slug' => str_slug($this->services['activities']),
                'module_type' => 'booking',
                'prefix' => 'CVACTB',
                'start' => 20000,
                'last_used' => 19999
            ],
            [
                'module' => $this->services['tailormadeTours'],
                'slug' => str_slug($this->services['tailormadeTours']),
                'module_type' => 'booking',
                'prefix' => 'CVTMB',
                'start' => 20000,
                'last_used' => 19999
            ]
        ];

        foreach ($codes as $code) {
            Code::firstOrCreate($code);
        }

        echo "Codes data is seeded" . PHP_EOL;
    }
}
