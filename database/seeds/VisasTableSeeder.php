<?php

use Admin\Visas\Models\Visa;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;


class VisasTableSeeder extends Seeder
{

    public function run() {
        $visas = [
            [
                'visa_type_id' => 1,
                'country_id' => 8,
                'nationality' => 104,
                'residence_country_id' => 104,
                'name' => '90 Day Visit Visa',
                'slug' => str_slug('90 Day Visit Visa')
            ]
        ];

        foreach ($visas as $visa) {
            Visa::firstOrCreate($visa);
        }

        $this->createVisaPricings();

        echo "Visa data is seeded" . PHP_EOL;
    }

    private function createVisaPricings() {
        $pricings = [
            [
                'title' => 'Child',
                'min_age' => 0,
                'max_age' => 12,
                'price' => 1000,
                'vfs_fee' => 2000,
                'other_charges' => 3000,
                'service_fee' => 4000,
                'other_charges_info' => 'Extra Service Fee :P'
            ],
            [
                'title' => 'Adult',
                'min_age' => 13,
                'price' => 5000,
                'vfs_fee' => 6000,
                'other_charges' => 7000,
                'service_fee' => 8000,
                'other_charges_info' => 'Extra Service Fee :P'
            ]
        ];
        $visa = Visa::first();
        foreach ($pricings as $pricing) {
            $visa->prices()->create($pricing);
        }
    }
}
