<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'Kiran Kumar',
                'last_name' => 'Vanam',
                'email' => 'kiran916.vanam@gmail.com',
                'password' => Hash::make('M@nalivanams2110'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 9248191991,
                'usergroup' => 'superuser',
            ],
            [
                'first_name' => 'Kamalakar Reddy',
                'last_name' => 'Chagam',
                'email' => 'chagamkamalakar@gmail.com',
                'password' => Hash::make('kamal@123'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 7093096349,
                'usergroup' => 'superuser',
            ],
            [
                'first_name' => 'Kiran Kumar',
                'last_name' => 'Vanam',
                'email' => 'kiran@columbusvacations.in',
                'password' => Hash::make('M@nalivanams1021'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 4066881991,
                'usergroup' => 'superadmin',
            ],
            [
                'first_name' => 'Satish Kumar',
                'last_name' => 'Kasanagottu',
                'email' => 'satish@columbusvacations.in',
                'password' => Hash::make('satish@123'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 9666691991,
                'usergroup' => 'superadmin',
            ],
            [
                'first_name' => 'Goutham Raj',
                'last_name' => 'Konthalapally',
                'email' => 'visas@columbusvacations.in',
                'password' => Hash::make('visas@goutham'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 8885218991,
                'usergroup' => 'admin',
            ],
            [
                'first_name' => 'Thirupathi',
                'last_name' => 'Reddy',
                'email' => 'tickets@columbusvacations.in',
                'password' => Hash::make('tickets@thirupathi'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 9542099191,
                'usergroup' => 'admin',
            ],
            [
                'first_name' => 'Naveen Kumar',
                'last_name' => 'Vanam',
                'email' => 'vanam413@gmail.com',
                'password' => Hash::make('vanam413'),
                'is_active' => true,
                'country_id' => 234,
                'calling_code' => 1,
                'phone' => 2056173217,
                'usergroup' => 'registeredcustomer',
            ],
            [
                'first_name' => 'Praveen Kumar',
                'last_name' => 'Vanam',
                'email' => 'praveenkumar03316@gmail.com',
                'password' => Hash::make('praveenkumarvanam@03316'),
                'is_active' => true,
                'country_id' => 234,
                'calling_code' => 1,
                'phone' => 2054709175,
                'usergroup' => 'registeredcustomer',
            ],
            [
                'first_name' => 'Sudheera',
                'last_name' => 'Vanam',
                'email' => 'sudheeravaraganti@gmail.com',
                'password' => Hash::make('sudheera@varaganti'),
                'is_active' => true,
                'country_id' => 234,
                'calling_code' => 1,
                'phone' => 2059034365,
                'usergroup' => 'guestcustomer',
            ],
            [
                'first_name' => 'Harika',
                'last_name' => 'Vanam',
                'email' => 'harika.rapolu@gmail.com',
                'is_active' => true,
                'country_id' => 234,
                'calling_code' => 1,
                'phone' => 2055206949,
                'usergroup' => 'subscriber',
            ],
            [
                'first_name' => 'Yadagiri',
                'last_name' => 'Vanam',
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 8143236089,
                'usergroup' => 'guestcustomer',
            ],
            [
                'first_name' => 'Manali Kiran',
                'last_name' => 'Vanam',
                'email' => 'manali.hotkar10@gmail.com',
                'password' => Hash::make('KiranMona1021'),
                'is_active' => true,
                'country_id' => 104,
                'calling_code' => 91,
                'phone' => 7387099942,
                'usergroup' => 'registeredcustomer',
            ]
        ];

        foreach ($users as $user) {
            $userObj = null;
            if(isset($user['email'])) {
                $userObj = User::where('email', $user['email'])->first();
            } else {
                $userObj = User::where('phone', $user['phone'])->first();
            }
            if(!$userObj) {
                User::create($user);
            }
        }
        echo "Users Data is seeded" . PHP_EOL;
    }
}
