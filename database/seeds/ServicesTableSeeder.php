<?php

use Admin\Settings\Models\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    protected $services;
    public function __construct () {
        $this->services = include('Services.php');
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
                'name' => $this->services['visas'],
                'slug' => str_slug($this->services['visas']),
                'availability_status' => 'active',
            ],
            [
                'name' => $this->services['activities'],
                'slug' => str_slug($this->services['activities']),
                'availability_status' => 'active',
            ],
            [
                'name' => $this->services['groupTours'],
                'slug' => str_slug($this->services['groupTours']),
                'availability_status' => 'active',
            ],
            [
                'name' => $this->services['packageTours'],
                'slug' => str_slug($this->services['packageTours']),
                'availability_status' => 'active',
            ],
            [
                'name' => $this->services['tailormadeTours'],
                'slug' => str_slug($this->services['tailormadeTours']),
                'availability_status' => 'inactive',
            ]
        ];

        foreach ($services as $service) {
            Service::firstOrCreate($service);
        }

        echo "Services Table is Seeded" . PHP_EOL;
    }
}
