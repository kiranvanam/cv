<?php

return array(
    'visas' => 'Visas',
    'activities' => 'Activities',
    'groupTours' => 'Group Tours',
    'packageTours' => 'Package Tours',
    'tailormadeTours' => 'Tailormade Tours'
);
