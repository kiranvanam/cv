<?php

use Admin\Visas\Models\VisaType;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;


class VisaTypesTableSeeder extends Seeder
{
    public function run() {
        $visaTypes = [
            [
                'name' => 'Visit Visa',
                'slug' => str_slug('Visit Visa')
            ],
            [
                'name' => 'Business Visa',
                'slug' => str_slug('Business Visa')
            ]
        ];

        foreach ($visaTypes as $visaType) {
            VisaType::firstOrCreate($visaType);
        }

        echo "Visa Types data is seeded" . PHP_EOL;
    }
}
