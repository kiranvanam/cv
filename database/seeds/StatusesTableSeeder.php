<?php

use Admin\Settings\Models\Status;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;


class StatusesTableSeeder extends Seeder
{
    public function run() {
        $enquiry = 'enquiry';
        $booking = 'booking';
        $availability = 'availability';
        $statuses = [
            [
                'name' => 'Pending',
                'slug' => str_slug('Pending'),
                'type' => $enquiry,
                'description' => "We haven't responded to the customer yet."
            ],
            [
                'name' => 'Awaiting Revert',
                'slug' => str_slug('Awaiting Revert'),
                'type' => $enquiry,
                'description' => "There is not revert from the customer yet. Waiting for response from the customer."
            ],
            [
                'name' => 'Processing',
                'slug' => str_slug('Processing'),
                'type' => $enquiry,
                'description' => "We are in talks with the customer for modifications (if any). Waiting for the confirmation from the customer."
            ],
            [
                'name' => 'Rejected',
                'slug' => str_slug('Rejected'),
                'type' => $enquiry,
                'description' => "The enquiry has been rejected / cancelled by the customer."
            ],
            [
                'name' => 'Booked',
                'slug' => str_slug('Booked'),
                'type' => $enquiry,
                'description' => "The enquiry has been successfully converted."
            ],
            [
                'name' => 'Completed',
                'slug' => str_slug('Completed'),
                'type' => $enquiry,
                'description' => "The enquiry has been successfully converted and the customer has successfully traveled."
            ],
            [
                'name' => 'Closed',
                'slug' => str_slug('Closed'),
                'type' => $enquiry,
                'description' => "The enquiry has been closed."
            ],
            [
                'name' => 'Unpaid',
                'slug' => str_slug('Unpaid'),
                'type' => $booking,
                'description' => "No payment received yet."
            ],
            [
                'name' => 'Partially Paid',
                'slug' => str_slug('Partially Paid'),
                'type' => $booking,
                'description' => "Only partial payment received."
            ],
            [
                'name' => 'Paid',
                'slug' => str_slug('Paid'),
                'type' => $booking,
                'description' => "Complete payment received."
            ],
            [
                'name' => 'Booking Cancelled',
                'slug' => str_slug('Booking Cancelled'),
                'type' => $booking,
                'description' => "The booking has been cancelled."
            ],
            [
                'name' => 'Booking Confirmed',
                'slug' => str_slug('Booking Confirmed'),
                'type' => $booking,
                'description' => "The booking has been confirmed."
            ],
            [
                'name' => 'Completed',
                'slug' => str_slug('Completed'),
                'type' => $booking,
                'description' => "The transaction is complete"
            ],
            [
                'name' => 'Active',
                'slug' => str_slug('Active'),
                'type' => $availability,
                'description' => "The queried resource is active"
            ],
            [
                'name' => 'Inactive',
                'slug' => str_slug('Inactive'),
                'type' => $availability,
                'description' => "The queried resource is inactive"
            ],
            [
                'name' => 'Hold',
                'slug' => str_slug('Hold'),
                'type' => $availability,
                'description' => "The queried resource is in hold"
            ]
        ];

        foreach ($statuses as $status) {
            Status::firstOrCreate($status);
        }

        echo "Status data is seeded" . PHP_EOL;
    }
}
