<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ServicesTableSeeder::class);
        $this->call(ResourceTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(VisasTableSeeder::class);
        $this->call(VisaTypesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(CodesTableSeeder::class);
    }
}
