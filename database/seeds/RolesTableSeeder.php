<?php

use Admin\Auth\Models\Role;
use Admin\Auth\Models\Permission;
use Admin\Auth\Models\Resource;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    protected $superUser = 'Super User';
    protected $superAdmin = 'Super Admin';
    protected $staff = 'Staff';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => $this->superUser,
                'slug' => str_slug($this->superUser),
                'module' => 'staff',
                'description' => 'You are the Software Creator',
                'owner_id' => 0
            ],
            [
                'name' => $this->superAdmin,
                'slug' => str_slug($this->superAdmin),
                'module' => 'staff',
                'description' => 'You are the Travel Agency Owner',
                'owner_id' => 1
            ],
            [
                'name' => $this->staff,
                'slug' => str_slug($this->staff),
                'module' => 'staff',
                'description' => 'You are the staff of Travel Agency',
                'owner_id' => 2
            ]
        ];

        foreach ($roles as $role) {
            Role::firstOrCreate($role);
        }

        // Software Owner
        $this->assignSuperUserPermissions();
        // Agency Owner
        $this->assignSuperAdminPermissions();

        echo "Roles with Permissions Data is seeded" . PHP_EOL;
    }

    // Software Owner
    private function assignSuperUserPermissions ()
    {
        $role = Role::where('slug', str_slug($this->superUser))->first();
        $permissions = Permission::all()->pluck('id');
        $role->permissions()->sync($permissions);
    }

    // Agency Owner
    private function assignSuperAdminPermissions ()
    {
        $role = Role::where('slug', str_slug($this->superAdmin))->first();
        $excludePermissions = Resource::where('slug', 'travel-agency')->first()->permissions->pluck('id')->toArray();
        $permissions = Permission::whereNotIn('id', $excludePermissions)->get()->pluck('id');
        $role->permissions()->sync($permissions);
    }
}
