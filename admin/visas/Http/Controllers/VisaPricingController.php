<?php

namespace Admin\Visas\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Admin\Visas\Http\Requests\VisaPricingCreateRequest;
use Admin\Visas\Models\Visa;
use Admin\Visas\Models\VisaPricing;

class VisaPricingController extends ApiController {
    public function post (VisaPricingCreateRequest $request)
    {
        $visa = Visa::find($request['visa_id']);
        $visaPrice = $visa->prices()->create($request->all());
        return $this->ok($visaPrice, "Visa Pricing Created Successfully");
    }

    public function update($id, VisaPricingCreateRequest $request) {
        $visaPricing = VisaPricing::find($id);
        $visaPricing->update($request->all());
        return $this->ok($visaPricing, "Visa Pricing Updated Successfully");
    }

    public function delete($id) {
        $visaPricing = VisaPricing::find($id);
        $visaPricing->delete();
        return $this->ok($visaPricing, "Visa Pricing Deleted Successfully");
    }
}
