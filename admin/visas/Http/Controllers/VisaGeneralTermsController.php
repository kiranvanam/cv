<?php

namespace Admin\Visas\Http\Controllers;

use Illuminate\Http\Request;
use Admin\Visas\Http\Requests\VisaCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Visas\Models\VisaGeneralTerms;

class VisaGeneralTermsController extends ApiController {
    public function update(Request $request) {
        $data = $request->all();
        $id = isset($data['id']) ? $data['id'] : NULL;
        $allTerms = VisaGeneralTerms::all();
        $firstId = count($allTerms) ? $allTerms[0]->id : NULL;
        if(count($allTerms) > 1 || $firstId != $id) {
            return $this->validationErrors($data, "Visa General Terms Already Added");
        }
        if($firstId) {
            $allTerms[0]->update($data);
            return $this->ok($allTerms[0], "Visa General Terms Updated Successfully");
        } else {
            return $this->ok(VisaGeneralTerms::create($request->all()), "Visa General Terms Added Successfully");
        }
    }

    public function delete($id) {
        $visaGeneralTerms = VisaGeneralTerms::find($id);
        $visaGeneralTerms->delete();
        return $this->ok($visaGeneralTerms, "Visa General Terms Deleted Successfully");
    }
}
