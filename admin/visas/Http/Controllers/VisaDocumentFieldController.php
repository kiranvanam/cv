<?php

namespace Admin\Visas\Http\Controllers;

use Admin\Visas\Http\Requests\VisaDocumentFieldCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Visas\Models\VisaDocumentField;

class VisaDocumentFieldController extends ApiController {
    public function post(VisaDocumentFieldCreateRequest $request) {
        return $this->ok(VisaDocumentField::create($request->all()), "Visa Document Field Created Successfully");
    }

    public function update($id, VisaDocumentFieldCreateRequest $request) {
        $visaDocumentField = VisaDocumentField::find($id);
        $visaDocumentField->update($request->all());
        return $this->ok($visaDocumentField, "Visa Document Field Updated Successfully");
    }

    public function delete($id) {
        $visaDocumentField = VisaDocumentField::find($id);
        $visaDocumentField->delete();
        return $this->ok($visaDocumentField, "Visa Document Field Deleted Successfully");
    }
}
