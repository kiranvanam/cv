<?php

namespace Admin\Visas\Http\Controllers;

use Admin\Visas\Http\Requests\VisaCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Visas\Models\VisaType;
use Admin\Visas\Models\VisaDocumentField;
use Admin\Visas\Models\VisaGeneralTerms;

class VisaSettingsController extends ApiController {
    public function fetch() {
        $visaTypes = VisaType::all();
        $visaGeneralTerms = VisaGeneralTerms::all();
        $visaDocumentFields = VisaDocumentField::all();
        return $this->ok([
            'visaTypes' => $visaTypes,
            'visaGeneralTerms' => $visaGeneralTerms,
            'visaDocumentFields' => $visaDocumentFields
        ], "Fetching All Visa Details Is Successful");
    }
}
