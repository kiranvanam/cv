<?php

namespace Admin\Visas\Http\Controllers;


use Illuminate\Http\Request;
use Admin\Visas\Http\Requests\VisaCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Visas\Models\Visa;
use Admin\Visas\Models\VisaCountry;
use Admin\Visas\Models\VisaMarketCountry;
use Admin\Visas\Models\VisaType;

class VisaController extends ApiController {
    public function fetch ()
    {
        $visas = Visa::with('prices')->get();
        return $this->ok($visas, "Fetching Visas Is Successful");
    }

    public function post(VisaCreateRequest $request) {
        return $this->ok(Visa::create($request->all()), "Visa Created Successfully");
    }

    public function update($id, VisaCreateRequest $request) {
        $visa = Visa::find($id);
        $visa->update($request->all());
        return $this->ok($visa, "Visa Updated Successfully");
    }

    public function delete($id) {
        $visa = Visa::find($id);
        $visa->delete();
        return $this->ok($visa, "Visa Deleted Successfully");
    }
}
