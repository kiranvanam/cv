<?php

namespace Admin\Visas\Http\Controllers;

use Admin\Visas\Http\Requests\VisaTypeCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Visas\Models\VisaType;

class VisaTypeController extends ApiController {
    public function post(VisaTypeCreateRequest $request) {
        return $this->ok(VisaType::create($request->all()), "Visa Type Created Successfully");
    }

    public function update($id, VisaTypeCreateRequest $request) {
        $visaType = VisaType::find($id);
        $visaType->update($request->all());
        return $this->ok($visaType, "Visa Type Updated Successfully");
    }

    public function delete($id) {
        $visaType = VisaType::find($id);
        $visaType->delete();
        return $this->ok($visaType, "Visa Type Deleted Successfully");
    }
}
