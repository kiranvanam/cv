<?php

namespace Admin\Visas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VisaCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'country_id' => 'required',
            'visa_type_id' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => str_slug($this->get('name'))
        ]);
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'name.required' => "Please Enter A Valid Name",
            'country_id.required' => "Please Select A Valid Country",
            'visa_type_id.required' => "Please Select A Valid Visa Type"
        ];
    }
}
