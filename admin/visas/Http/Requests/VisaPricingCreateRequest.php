<?php

namespace Admin\Visas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class VisaPricingCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'visa_id' => 'required',
            'price' => 'required',
            'service_fee' => 'required',
            'min_age' => 'required'
        ];
    }

    public function withValidator($validator)
    {
        $all = $this->all();
        $validator->after(function ($validator) {
            if(isset($all['other_charges']) && !isset($all['other_charges_info']))
            {
                $validator->errors()->add('other_charges_info', 'Please enter description for other charges');
            }
        });
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'visa_id.required' => "Please Select A Valid Visa",
            'title.required' => "Please Enter Title",
            'min_age.required' => "Please Enter Minimum Age",
            'price.required' => "Please Enter A Value For Price",
            'service_fee.required' => "Please Enter Service Fee"
        ];
    }
}
