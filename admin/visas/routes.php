<?php

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'visas'], function () {
        Route::group(['prefix' => 'settings'], function () {
            Route::get('', 'VisaSettingsController@fetch');

            // Routes specific to visa types
            Route::group(['prefix' => 'visa-type'], function () {
                Route::post('/', 'VisaTypeController@post');
                Route::put('/{id}', 'VisaTypeController@update');
                Route::delete('/{id}', 'VisaTypeController@delete');
            });

            // Routes specific to visa document fields
            Route::group(['prefix' => 'visa-document-field'], function () {
                Route::post('/', 'VisaDocumentFieldController@post');
                Route::put('/{id}', 'VisaDocumentFieldController@update');
                Route::delete('/{id}', 'VisaDocumentFieldController@delete');
            });

            // Routes specific to visa general terms & conditions
            Route::group(['prefix' => 'visa-general-terms-conditions'], function () {
                Route::post('/', 'VisaGeneralTermsController@update');
            });
        });
        Route::get('', 'VisaController@fetch');
        Route::post('', 'VisaController@post');
        Route::put('/{id}', 'VisaController@update');
        Route::delete('/{id}', 'VisaController@delete');

        // Routes specific to visa pricing
        Route::group(['prefix' => 'pricing'], function () {
            Route::post('', 'VisaPricingController@post');
            Route::put('/{id}', 'VisaPricingController@update');
            Route::delete('/{id}', 'VisaPricingController@delete');
        });
    });
});
