<?php

namespace Admin\Visas\Models;

use Admin\Visas\Models\Visa;
use Illuminate\Database\Eloquent\Model;

class VisaPricing extends Model
{
    public $timestamps = false;
    protected $fillable = ['visa_id', 'price', 'vfs_fee', 'other_charges', 'service_fee', 'min_age', 'max_age', 'title', 'other_charges_info'];

    public function visa ()
    {
        return $this->belongsTo(Visa::class);
    }
}
