<?php

namespace Admin\Visas\Models;

use Illuminate\Database\Eloquent\Model;
use Admin\Visas\Models\VisaPricing;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visa extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [ 'country_id', 'nationality', 'residence_country_id', 'visa_type_id',
                        'name', 'slug', 'apply_duration', 'processing_time', 'validity_period', 'conditions',
                        'required_documents', 'notes' ];
    protected $casts = [ 'required_documents' => 'array' ];

    public function prices () {
        return $this->hasMany(VisaPricing::class);
    }
}
