<?php

namespace Admin\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaDocumentField extends Model
{
    public $timestamps = false;
    protected $fillable = [ 'name', 'slug' ];
}
