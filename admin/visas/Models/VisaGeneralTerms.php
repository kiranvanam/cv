<?php

namespace Admin\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaGeneralTerms extends Model
{
    public $timestamps = false;
    protected $fillable = ['terms_conditions'];
}
