<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visa_id');
            $table->string('title');
            $table->float('price', 8, 2);
            $table->float('vfs_fee', 8, 2)->nullable();
            $table->float('other_charges', 8, 2)->nullable();
            $table->float('service_fee', 8, 2);
            $table->integer('min_age');
            $table->integer('max_age')->nullable();
            $table->text('other_charges_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_pricings');
    }
}
