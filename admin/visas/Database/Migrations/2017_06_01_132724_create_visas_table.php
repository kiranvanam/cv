<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('nationality')->default(104);
            $table->integer('residence_country_id')->default(104);
            $table->integer('visa_type_id');
            $table->string('name');
            $table->string('slug');
            $table->text('apply_duration')->nullable();
            $table->text('processing_time')->nullable();
            $table->text('validity_period')->nullable();
            $table->text('conditions')->nullable();
            $table->text('notes')->nullable();
            $table->json('required_documents')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visas');
    }
}
