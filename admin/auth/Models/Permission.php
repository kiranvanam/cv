<?php

namespace Admin\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'resource_id'];
}
