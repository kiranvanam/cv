<?php

namespace Admin\Auth\Models;

use App\User;
use Admin\Auth\Models\Permission;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'user_id', 'name', 'slug', 'owner_id' ];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
