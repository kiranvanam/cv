<?php

namespace Admin\Auth\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $namespace = "Admin\Auth\Http\Controllers";

    public function boot () {
        $this->app->router->group([ 'namespace' => $this->namespace ], function () {
            if (!$this->app->routesAreCached())
            {
                require __DIR__.'/../routes.php';
            }
        });
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }
}
