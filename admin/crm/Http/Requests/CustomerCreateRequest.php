<?php

namespace Admin\Crm\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'country_id' => 'required_with:phone',
            'calling_code' => 'required_with:phone',
            'email' => 'required_without:phone|email',
            'phone' => 'required_without:email'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'group' => 'guestcustomer',
            'subscriber' => true,
            'is_active' => true
        ]);
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'first_name.required' => "Please Enter A Valid First Name",
        ];
    }
}
