<?php

namespace Admin\Crm\Http\Controllers;

use App\User;
use Admin\Crm\Http\Requests\CustomerCreateRequest;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class CustomerController extends ApiController {
    // This class is used to add / update / fetch guest customers
    public function search (Request $request)
    {
        $query = User::customerFilter();
        $query = $query->where('is_active', true);
        if(isset($request['first_name'])) {
            $query = $query->where('first_name', 'like', '%' . $request['first_name'] . '%');
        }
        if(isset($request['country_id'])) {
            $query = $query->where('country_id', $request['country_id']);
        }
        if(isset($request['email'])) {
            $query = $query->where('email', 'like', '%' . $request['email'] . '%');
        }
        if(isset($request['phone'])) {
            $query = $query->where('email', 'like', '%' . $request['phone'] . '%');
        }
        return $this->ok($query->get(), "Guest Customers Fetched Successfully");
    }

    public function fetch(Request $request)
    {
        $queryString = $request['query'];
        $customers = [];
        // return ["body" => ['Kiran Kumar', 'Vanam']];
        if(!empty($queryString)) {
            if(is_numeric($queryString)) {
                $customers = User::where('phone', 'like', "%$queryString%")->get();
            } else {
                $customers = User::where('first_name', 'like', "%$queryString%")
                                ->orWhere('last_name', 'like', "%$queryString%")
                                ->orWhere('email', 'like', "%$queryString%")
                                ->get();
            }
        }

        return $this->ok($customers, "Fetching Customers Is Successful");

    }

    public function post(CustomerCreateRequest $request) {
        // Only Guest Customers
        return $this->ok(User::create($request->all()), "Guest Customer Created Successfully");
    }

    public function update($id, CustomerCreateRequest $request) {
        $user = User::find($id);
        $user->update($request->all());
        return $this->ok($user, "Guest Customer Updated Successfully");
    }

    public function delete($id) {
        $user = User::find($id);
        $user->delete();
        return $this->ok($user, "Guest Customer Deleted Successfully");
    }
}
