<?php

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'crm'], function () {
        // Route::get('', 'SystemSettingsController@fetch');
        Route::group(['prefix' => 'customer'], function () {
            // Search Customers To View / Update / Delete
            Route::post('search', 'CustomerController@search');
            // Fetch Customers For Tasks like selecting a customer to post an enquiry
            Route::post('fetch', 'CustomerController@fetch');
            Route::post('/', 'CustomerController@post');
            Route::put('/{id}', 'CustomerController@update');
            Route::delete('/{id}', 'CustomerController@delete');
        });
    });
});
