<?php

namespace Admin\Enquiries\Http\Controllers;

use App\User;
use App\Code;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Admin\Visas\Models\Visa;
use Admin\Visas\Models\VisaPricing;
use Admin\Settings\Models\Status;
use Admin\Enquiries\Http\Requests\VisaEnquiryCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Enquiries\Models\VisaEnquiry;
use Admin\Enquiries\Models\VisaBookingPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VisaEnquiryController extends ApiController {
    private function prepareSearchQuery($request)
    {
        $search = $request->all();
        $query = VisaEnquiry::with('customer')->with('creator');
        if(isset($search['customer_id'])) {
            $query = $query->where('customer_id', $search['customer_id']);
        }
        if(isset($search['code'])) {
            $query = $query->where('code', 'like', '%' . $search['code'] . '%');
        }
        if(isset($search['name'])) {
            $query = $query->where('name', 'like', '%' . $search['name'] . '%');
        }
        if(isset($search['country_id'])) {
            $query = $query->where('country_id', $search['country_id']);
        }
        if(isset($search['enquiry_date_from'])) {
            $enquiry_date_to = isset($search['enquiry_date_to']) ? $search['enquiry_date_to'] : Carbon::today();
            $query = $query->whereBetween('created_at', [$search['enquiry_date_from'], $enquiry_date_to]);
        }
        if(isset($search['travel_date_after'])) {
            $travel_date_before = isset($search['travel_date_before']) ? $search['travel_date_before'] : Carbon::today();
            $query = $query->whereBetween('from_date', [$search['travel_date_after'], $travel_date_before]);
        }
        if(isset($search['created_by'])) {
            $query = $query->where('created_by', $search['created_by']);
        }
        if(isset($search['assigned_to'])) {
            $query = $query->where('assigned_to', $search['assigned_to']);
        }
        if(isset($search['statuses']) && !empty($search['statuses'])) {
            $query = $query->whereIn('status', $search['statuses']);
        }
        return $query;
    }

    public function fetch (Request $request)
    {
        $query = $this->prepareSearchQuery($request);
        $visaEnquiries = $query->get();
        return $this->ok($visaEnquiries, "Fetching Visa Enquiries Is Successful");
    }

    public function post(VisaEnquiryCreateRequest $request) {
        $data = $request->all();
        $data['code'] = Code::getVisaEnquiryCode()->getNextCode();
        $data['created_at'] = Carbon::today()->toDateString();
        $data['updated_at'] = $data['created_at'];
        $data = $this->prepareUserData($data);
        return $this->ok(VisaEnquiry::create($data), "Visa Enquiry Created Successfully");
    }

    public function update($id, VisaEnquiryCreateRequest $request) {
        $visaEnquiry = VisaEnquiry::find($id);
        $request['updated_at'] = Carbon::today()->toDateString();
        $visaEnquiry->update($request->all());
        return $this->ok($visaEnquiry, "Visa Enquiry Updated Successfully");
    }

    public function delete($id) {
        $visaEnquiry = VisaEnquiry::find($id);
        $visaEnquiry->delete();
        return $this->ok($visaEnquiry, "Visa Enquiry Deleted Successfully");
    }

    public function book($id) {
        // Find VisaEnquiry, Visa & Visa Prices
        $visaEnquiry = VisaEnquiry::find($id);
        $visa = Visa::find($visaEnquiry->visa_id);
        $visaPrices = $visa->prices;

        // Prepare the booking Data
        $data = $this->prepareBookingData($visaEnquiry, $visa);

        // Create the booking
        $visaBooking = $visaEnquiry->booking()->create($data);

        // Update VisaEnquiry's booking_code
        $visaEnquiry->booking_code = $visaBooking->code;
        $visaEnquiry->save();

        // Make a copy of all the VisaPricing of the Visa as VisaBookingPrice
        $this->updateVisaBookingPrices($visaEnquiry, $visaPrices, $visaBooking);
        $visaEnquiry = VisaEnquiry::where('id', $visaEnquiry->id)->with('creator')->with('customer')->first();
        return $this->ok($visaEnquiry, "Visa Booking Created Successfully");
    }

    private function updateVisaBookingPrices ($visaEnquiry, $visaPrices, $visaBooking)
    {
        $bookingPrices = [];
        foreach ($visaEnquiry['traveler_info'] as $info) {
            foreach($visaPrices as $price) {
                if($price->id == $info['visa_pricing_id']) {
                    $bookingPrices[] = $this->prepareVisaBookingPrice($price, $info, $visaBooking);
                    break;
                }
            }
        }
        if(!empty($bookingPrices)) {
            $visaBooking->prices()->createMany($bookingPrices);
        }
        return;
    }

    private function prepareVisaBookingPrice ($price, $info, $visaBooking)
    {
        $visaPrice = [
            'visa_id' => $price->visa_id,
            'title' => $price->title,
            'price' => $price->price,
            'vfs_fee' => $price->vfs_fee,
            'other_charges' => $price->other_charges,
            'service_fee' => $price->service_fee,
            'min_age' => $price->min_age,
            'max_age' => $price->max_age,
            'other_charges_info' => $price->other_charges_info,
            'no_of_persons' => $info['no_of_persons'],
            'visa_price_id' => $price->id,
        ];
        return $visaPrice;
    }

    private function prepareBookingData ($visaEnquiry, $visa)
    {
        $now = Carbon::today()->toDateString();
        $status = Status::where('slug', 'unpaid')->first();
        $data = [
            'code' => Code::getVisaBookingCode()->getNextCode(),
            'created_at' => $now,
            'updated_at' => $now,
            'customer_id' => $visaEnquiry['customer_id'],
            'country_id' => $visaEnquiry['country_id'],
            'from_date' => $visaEnquiry['from_date'],
            'to_date' => $visaEnquiry['to_date'],
            'is_tentative' => $visaEnquiry['is_tentative'],
            'visa_id' => $visaEnquiry['visa_id'],
            'status' => $status->id
        ];
        $data = $this->prepareUserData($data);
        return $data;
    }

    private function prepareUserData($data)
    {
        $user = Auth::check() ? Auth::user() : NULL;
        $data['created_by'] = $user ? $user->id : NULL;
        if($user && $user->isAdmin()) {
            $data['assigned_to'] = $user->id;
        } else {
            $admin = User::where('usergroup', 'superadmin')->first();
            $data['assigned_to'] = $admin->id;
        }
        return $data;
    }
}
