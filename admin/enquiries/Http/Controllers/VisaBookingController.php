<?php

namespace Admin\Enquiries\Http\Controllers;

use App\User;
use App\Code;
use Carbon\Carbon;
use Admin\Enquiries\Models\VisaBooking;
use Admin\Enquiries\Models\VisaBookingPrice;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VisaBookingController extends ApiController {
    private function prepareSearchQuery($request)
    {
        $search = $request->all();
        $query = VisaBooking::with('customer')->with('creator')->with('prices');
        if(isset($search['customer_id'])) {
            $query = $query->where('customer_id', $search['customer_id']);
        }
        if(isset($search['code'])) {
            $query = $query->where('code', 'like', '%' . $search['code'] . '%');
        }
        if(isset($search['country_id'])) {
            $query = $query->where('country_id', $search['country_id']);
        }
        if(isset($search['booking_date_from'])) {
            $booking_date_to = isset($search['booking_date_to']) ? $search['booking_date_to'] : Carbon::today();
            $query = $query->whereBetween('created_at', [$search['booking_date_from'], $booking_date_to]);
        }
        if(isset($search['travel_date_after'])) {
            $travel_date_before = isset($search['travel_date_before']) ? $search['travel_date_before'] : Carbon::today();
            $query = $query->whereBetween('from_date', [$search['travel_date_after'], $travel_date_before]);
        }
        if(isset($search['created_by'])) {
            $query = $query->where('created_by', $search['created_by']);
        }
        if(isset($search['assigned_to'])) {
            $query = $query->where('assigned_to', $search['assigned_to']);
        }
        if(isset($search['statuses']) && !empty($search['statuses'])) {
            $query = $query->whereIn('status', $search['statuses']);
        }
        return $query;
    }

    public function fetch (Request $request)
    {
        $query = $this->prepareSearchQuery($request);
        $visaBookings = $query->get();
        return $this->ok($visaBookings, "Fetching Visa Bookings Is Successful");
    }

    public function post(Request $request) {
        $data = $request->all();
        $data['code'] = Code::getVisaBookingCode()->getNextCode();
        $data['created_at'] = Carbon::today()->toDateString();
        $data['updated_at'] = $data['created_at'];
        $data = $this->prepareUserData($data);
        $visaBooking = VisaBooking::create($data);
        if($data['prices']) {
            $visaBooking->prices()->createMany($data['prices']);
        }
        return $this->ok($visaBooking, "Visa Booking Created Successfully");
    }

    private function prepareUserData($data)
    {
        $user = Auth::check() ? Auth::user() : NULL;
        $data['created_by'] = $user ? $user->id : NULL;
        if($user && $user->isAdmin()) {
            $data['assigned_to'] = $user->id;
        } else {
            $admin = User::where('usergroup', 'superadmin')->first();
            $data['assigned_to'] = $admin->id;
        }
        return $data;
    }

    public function update($id, Request $request) {
        $visaBooking = VisaBooking::find($id);
        $request['updated_at'] = Carbon::today()->toDateString();
        $visaBooking->update($request->all());
        $this->updateVisaBookingPrices($visaBooking, $request['prices']);
        $visaBooking = VisaBooking::where('id', $visaBooking->id)->with('creator')->with('customer')->with('prices')->first();
        return $this->ok($visaBooking, "Visa Booking Updated Successfully");
    }

    public function delete($id) {
        $visaBooking = VisaBooking::find($id);
        $visaBooking->delete();
        // To Do: Delete visa booking prices ?
        return $this->ok($visaBooking, "Visa Booking Deleted Successfully");
    }

    private function updateVisaBookingPrices($visaBooking, $prices) {
        VisaBookingPrice::where('booking_code', $visaBooking->code)->delete();
        $visaBooking->prices()->createMany($data['prices']);
        /*foreach($prices as $price) {
            $priceModel = VisaBookingPrice::find($price['id']);
            $priceModel->update($price);
        }*/
    }
}
