<?php

namespace Admin\Enquiries\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VisaEnquiryCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'created_by' => 'sometimes',
            'assigned_to' => 'sometimes',
            'name' => 'required',
            'from_date' => 'required',
            'status' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([]);
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'country_id.required' => "Please Select A Valid Country",
            'name.required' => "Please Enter A Valid Enquiry Name",
            'visa_id.required' => "Please Select A Valid Visa",
            'from_date.required' => "Please Select A Valid Date of Travel",
            'status.required' => "Invalid Enquiry Status",
        ];
    }
}
