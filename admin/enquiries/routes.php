<?php

Route::group(['prefix' => 'api', 'middleware' => 'api'], function () {
    // Enquiry Routes
    Route::group(['prefix' => 'enquiries'], function () {
        Route::group(['prefix' => 'visa'], function () {
            Route::post('search', 'VisaEnquiryController@fetch');
            Route::post('/', 'VisaEnquiryController@post');
            Route::put('/{id}', 'VisaEnquiryController@update');
            Route::delete('/{id}', 'VisaEnquiryController@delete');
            Route::post('/book/{id}', 'VisaEnquiryController@book');
        });
    });

    // Booking Routes
    Route::group(['prefix' => 'bookings'], function () {
        Route::group(['prefix' => 'visa'], function () {
            Route::post('search', 'VisaBookingController@fetch');
            Route::post('/', 'VisaBookingController@post');
            Route::put('/{id}', 'VisaBookingController@update');
            Route::delete('/{id}', 'VisaBookingController@delete');
        });
    });
});
