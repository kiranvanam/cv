<?php

namespace Admin\Enquiries\Models;

use Admin\Enquiries\Models\VisaBooking;
use Illuminate\Database\Eloquent\Model;

class VisaBookingPrice extends Model
{
    public $timestamps = false;
    protected $fillable = ['visa_id', 'price', 'vfs_fee', 'other_charges', 'visa_price_id',
                        'service_fee', 'min_age', 'max_age', 'title',
                        'other_charges_info', 'no_of_persons', 'booking_code'];
    public function booking()
    {
        return $this->belongsTo(VisaBooking::class, 'booking_code', 'code');
    }
}
