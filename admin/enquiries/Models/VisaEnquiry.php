<?php

namespace Admin\Enquiries\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisaEnquiry extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'from_date', 'to_date'];

    protected $fillable = ['code', 'name', 'customer_id', 'visa_id', 'booking_code', 'country_id',
                'from_date', 'to_date', 'traveler_info', 'is_tentative', 'created_by',
                'assigned_to', 'status', 'notes', 'created_at', 'updated_at'];

    protected $dateFormat = 'Y-m-d';

    protected $casts = [
        'traveler_info' => 'array',
    ];

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function booking()
    {
        return $this->hasOne(VisaBooking::class, 'enquiry_code', 'code');
    }
}
