<?php

namespace Admin\Enquiries\Models;

use App\User;
use Admin\Enquiries\Models\VisaBooking;
use Admin\Enquiries\Models\VisaBookingPrice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisaBooking extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $dates = ['from_date', 'to_date', 'created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['code', 'customer_id', 'visa_id', 'enquiry_code', 'customer_id', 'country_id',
            'created_by', 'assigned_to', 'status', 'notes', 'created_at', 'updated_at',
            'from_date', 'to_date', 'discount'];

    protected $dateFormat = 'Y-m-d';

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function enquiry()
    {
        return $this->belongsTo(VisaBooking::class, 'enquiry_code', 'code');
    }

    public function prices()
    {
        return $this->hasMany(VisaBookingPrice::class, 'booking_code', 'code');
    }
}
