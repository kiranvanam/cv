<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('customer_id')->nullable();
            $table->integer('visa_id')->nullable();
            $table->string('enquiry_code')->unique()->nullable();
            $table->string('code')->unique();
            $table->integer('status')->index(); // Status of the enquiry - Look at the table statuses
            $table->date('from_date');
            $table->date('to_date');
            $table->date('created_at');
            $table->date('updated_at');
            $table->integer('created_by')->nullable(); // Staff / Customer who created the booking
            $table->integer('assigned_to')->index(); // Staff on whose name the booking is.
            $table->softDeletes();
            $table->float('discount', 8, 2)->default(0);
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_bookings');
    }
}
