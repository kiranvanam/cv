<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_enquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique(); // Auto generated code - CVVEXXXXX
            $table->string('name'); // Name of the enquiry
            $table->integer('customer_id')->nullable(); // Customer who enquired for a visa
            $table->integer('country_id');
            $table->integer('visa_id')->nullable(); // Visa
            $table->string('booking_code')->unique()->nullable(); // Booking Code
            $table->json('traveler_info')->nullable();
            $table->date('from_date');
            $table->date('to_date')->nullable();
            $table->boolean('is_tentative')->default(true);
            $table->integer('created_by')->nullable(); // Staff / Customer who created the enquiry
            $table->integer('assigned_to')->index(); // Staff on whose name the enquiry is.
            $table->integer('status')->index(); // Status of the enquiry - Look at the table statuses
            $table->text('notes')->nullable();
            $table->date('created_at');
            $table->date('updated_at');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_enquiries');
    }
}
