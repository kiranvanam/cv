<?php

namespace Admin\Enquiries\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class EnquiryServiceProvider extends ServiceProvider
{
    protected $namespace = "Admin\Enquiries\Http\Controllers";

    public function boot () {
        $this->app->router->group([ 'namespace' => $this->namespace ], function () {
            if (!$this->app->routesAreCached())
            {
                require __DIR__.'/../routes.php';
            }
        });
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }
}
