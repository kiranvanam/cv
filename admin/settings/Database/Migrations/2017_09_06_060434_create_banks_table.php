<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('city_name')->nullable();
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('account_holder_name');
            $table->bigInteger('account_no');
            $table->string('ifsc_code')->nullable();
            $table->string('micr_code')->nullable();
            $table->string('swift_code')->nullable();
            $table->string('other_code_title')->nullable();
            $table->string('other_code')->nullable();
            $table->text('address');
            $table->integer('pin_code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
