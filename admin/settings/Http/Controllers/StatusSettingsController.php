<?php

namespace Admin\Settings\Http\Controllers;

use Admin\Settings\Http\Requests\StatusSettingsCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Settings\Models\Status;

class StatusSettingsController extends ApiController {
    public function fetch ()
    {
        return $this->ok(Status::all(), "Status Settings Fetched Successfully");
    }

    public function post(StatusSettingsCreateRequest $request) {
        return $this->ok(Status::create($request->all()), "Status Setting Created Successfully");
    }

    public function update($id, StatusSettingsCreateRequest $request) {
        $status = Status::find($id);
        $status->update($request->all());
        return $this->ok($status, "Status Setting Updated Successfully");
    }

    public function delete($id) {
        $status = Status::find($id);
        $status->delete();
        return $this->ok($status, "Status Setting Deleted Successfully");
    }
}
