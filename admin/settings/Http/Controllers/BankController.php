<?php

namespace Admin\Settings\Http\Controllers;

use Admin\Settings\Http\Requests\BankCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Settings\Models\Bank;

class BankController extends ApiController {
    public function fetch ()
    {
        return $this->ok(Bank::all(), "Banks Fetched Successfully");
    }

    public function post(BankCreateRequest $request) {
        return $this->ok(Bank::create($request->all()), "Bank Created Successfully");
    }

    public function update($id, BankCreateRequest $request) {
        $bank = Bank::find($id);
        $bank->update($request->all());
        return $this->ok($bank, "Bank Updated Successfully");
    }

    public function delete($id) {
        $bank = Bank::find($id);
        $bank->delete();
        return $this->ok($bank, "Bank Deleted Successfully");
    }
}
