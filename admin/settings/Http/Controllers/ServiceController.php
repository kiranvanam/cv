<?php

namespace Admin\Settings\Http\Controllers;

use App\Constants\AvailabilityStatus;
use Admin\Settings\Http\Requests\ServiceCreateRequest;
use App\Http\Controllers\ApiController;
use Admin\Settings\Models\Service;

class ServiceController extends ApiController {
    public function fetch ()
    {
        return $this->ok(Service::all(), "Services Fetched Successfully");
    }

    public function post(ServiceCreateRequest $request) {
        $request['availability_status'] = AvailabilityStatus::ACTIVE;
        return $this->ok(Service::create($request->all()), "Service Created Successfully");
    }

    public function update($id, ServiceCreateRequest $request) {
        $service = Service::find($id);
        $service->update($request->all());
        return $this->ok($service, "Service Updated Successfully");
    }

    public function delete($id) {
        $service = Service::find($id);
        $service->delete();
        return $this->ok($service, "Service Deleted Successfully");
    }
}
