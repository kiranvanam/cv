<?php

namespace Admin\Settings\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Constants\AvailabilityStatus;
use Admin\Settings\Models\Status;
use Admin\Settings\Models\Service;
use Admin\Settings\Models\Bank;
use App\User;

class SystemSettingsController extends ApiController {
    public function fetch() {
        $statuses = Status::all();
        $admins = User::fetchAdmins();
        $services = Service::all();
        $banks = Bank::all();
        return $this->ok([
            'services' => $services,
            'statuses' => $statuses,
            'admins' => $admins,
            'banks' => $banks
        ], "Fetching All System Settings Is Successful");
    }
}
