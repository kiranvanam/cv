<?php

namespace Admin\Settings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BankCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'city_name' => 'required',
            'bank_name' => 'required',
            'branch_name' => 'required',
            'account_holder_name' => 'required',
            'account_no' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'bank_name.required' => "Please Enter A Valid Bank Name",
            'branch_name.required' => "Please Enter A Valid Branch Name",
            'account_holder_name.required' => "Please Enter A Valid Account Holder Name",
            'account_no.required' => "Please Enter A Valid Account Number",
            'city_name.required' => "Please Enter A Valid City Name",
            'country_id.required' => "Please Select A Valid Country"
        ];
    }
}
