<?php

namespace Admin\Settings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StatusSettingsCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required',
            'description' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => str_slug($this->get('name'))
        ]);
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'name.required' => "Please Enter A Valid Name",
            'type.required' => "Please Select A Valid Type",
            'description.required' => "Please Enter Valid Description For The Status"
        ];
    }
}
