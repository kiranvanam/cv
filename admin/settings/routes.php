<?php

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'system-settings'], function () {
        Route::get('', 'SystemSettingsController@fetch');
        Route::group(['prefix' => 'status'], function () {
            Route::get('', 'StatusSettingsController@fetch');
            Route::post('/', 'StatusSettingsController@post');
            Route::put('/{id}', 'StatusSettingsController@update');
            Route::delete('/{id}', 'StatusSettingsController@delete');
        });
        Route::group(['prefix' => 'service'], function () {
            Route::post('/', 'ServiceController@post');
            Route::put('/{id}', 'ServiceController@update');
            Route::delete('/{id}', 'ServiceController@delete');
        });
        Route::group(['prefix' => 'bank'], function () {
            Route::post('/', 'BankController@post');
            Route::put('/{id}', 'BankController@update');
            Route::delete('/{id}', 'BankController@delete');
        });
    });
});
