<?php

namespace Admin\Settings\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    protected $namespace = "Admin\Settings\Http\Controllers";

    public function boot () {
        $this->app->router->group([ 'namespace' => $this->namespace ], function () {
            if (!$this->app->routesAreCached())
            {
                require __DIR__.'/../routes.php';
            }
        });
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }
}
