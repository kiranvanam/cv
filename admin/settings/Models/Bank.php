<?php

namespace Admin\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['country_id', 'city_name', 'bank_name', 'branch_name', 'account_holder_name',
            'account_no', 'ifsc_code', 'micr_code', 'swift_code', 'other_code_title', 'other_code',
            'address', 'pin_code'];
}
