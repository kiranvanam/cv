<?php

namespace Admin\Settings\Models;

use Illuminate\Database\Eloquent\Model;
use Admin\Locations\Models\Country;
use Admin\Locations\Models\Destination;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'slug', 'availability_status'];

    public function countries ()
    {
        return $this->morphByMany(Country::class, 'serviceable');
    }

    public function destinations ()
    {
        return $this->morphByMany(Destination::class, 'serviceable');
    }
}
