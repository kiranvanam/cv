<?php

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'countries'], function () {
        Route::get('', 'CountryController@all');
        Route::put('/{id}', 'CountryController@update');
        Route::put('/services/{id}', 'CountryController@updateServices');
    });
});
