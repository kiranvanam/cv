<?php

namespace Admin\Locations\Http\Controllers;

use App\Http\Controllers\ApiController;
use Admin\Locations\Models\Country;
use Illuminate\Http\Request;

class CountryController extends ApiController
{
    public function all()
    {
        $countries = Country::with('services')
                            ->orderBy('name', 'asc')->get();
        return $this->ok($countries, "Countries List");
    }

    public function update($id, Request $request)
    {
        $country = Country::find($id);
        $country->update($request->all());
        return $this->ok($country, "Country Updated Successfully");
    }

    public function updateServices($id, Request $request)
    {
        $country = Country::find($id);
        $country->services()->sync($request['services']);
        $country['services'] = $country->services;
        return $this->ok($country, "Country Updated Successfully");
    }
}
