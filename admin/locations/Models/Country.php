<?php

namespace Admin\Locations\Models;

use Illuminate\Database\Eloquent\Model;
use Admin\Settings\Models\Service;

class Country extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'nationality', 'cca2', 'cca3', 'currency',
        'calling_code', 'region', 'subregion', 'lat', 'lng', 'currency_id', 'searchable_terms'];
    protected $casts = [
        'currency' => 'array',
        'calling_code' => 'array'
    ];

    public function services ()
    {
        return $this->morphToMany(Service::class, 'serviceable');
    }
}
