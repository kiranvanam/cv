<?php

return [
    "resource" => [ "name" => "Travel Agency", "slug" => "travel-agency"],
    "permissions" => [
        ["name" => "List View", "slug" => "list"],
        ["name" => "View details", "slug" => "details"],
        ["name" => "Edit",  "slug" => "edit"],
        ["name" => "Create", "slug" => "create"],
        ["name" => "Delete", "slug" => "delete"]
      ],
    "sub" => []
];
