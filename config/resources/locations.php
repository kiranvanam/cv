<?php

return [
    "resource" => [ "name" => "Locations", "slug" => "locations"],
    "permissions" => [],
    "sub" => [
      [
        "resource" => [ "name" => "Visa Countries", "slug" => "visa-countries"],
        "permissions" => [
            ["name" => "List View", "slug" => "list"],
            ["name" => "View details", "slug" => "details"],
            ["name" => "Edit",  "slug" => "edit"],
            ["name" => "Create", "slug" => "create"],
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ],
      [
        "resource" => [ "name" => "Visa Market Countries", "slug" => "visa-market-countries"],
        "permissions" => [
            ["name" => "List View", "slug" => "list"],
            ["name" => "View details", "slug" => "details"],
            ["name" => "Edit",  "slug" => "edit"],
            ["name" => "Create", "slug" => "create"],
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ]
    ]
];
