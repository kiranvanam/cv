<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Columbus - @yield('title')</title>
        <link href="../../modules/core/common/img/favicon.ico" rel="shortcut icon">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- VENDORS -->
        <!-- v2.0.0 -->
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/jscrollpane/style/jquery.jscrollpane.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/ladda/dist/ladda-themeless.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/select2/dist/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/bootstrap-sweetalert/dist/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/summernote/dist/summernote.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/ionrangeslider/css/ion.rangeSlider.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/datatables/media/css/dataTables.bootstrap4.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/c3/c3.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/chartist/dist/chartist.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/nprogress/nprogress.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/jquery-steps/demo/css/jquery.steps.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/dropify/dist/css/dropify.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/font-linearicons/style.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/font-icomoon/style.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/cleanhtmlaudioplayer/src/player.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/vendors/cleanhtmlvideoplayer/src/player.css">
        <script src="/cvadmin/vendors/jquery/dist/jquery.min.js"></script>
        <script src="/cvadmin/vendors/tether/dist/js/tether.min.js"></script>
        <script src="/cvadmin/vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="/cvadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/cvadmin/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
        <script src="/cvadmin/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
        <script src="/cvadmin/vendors/spin.js/spin.js"></script>
        <script src="/cvadmin/vendors/ladda/dist/ladda.min.js"></script>
        <script src="/cvadmin/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="/cvadmin/vendors/select2/dist/js/select2.full.min.js"></script>
        <script src="/cvadmin/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
        <script src="/cvadmin/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
        <script src="/cvadmin/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
        <script src="/cvadmin/vendors/autosize/dist/autosize.min.js"></script>
        <script src="/cvadmin/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
        <script src="/cvadmin/vendors/moment/min/moment.min.js"></script>
        <script src="/cvadmin/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="/cvadmin/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="/cvadmin/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
        <script src="/cvadmin/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <script src="/cvadmin/vendors/summernote/dist/summernote.min.js"></script>
        <script src="/cvadmin/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="/cvadmin/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
        <script src="/cvadmin/vendors/nestable/jquery.nestable.js"></script>
        <script src="/cvadmin/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="/cvadmin/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
        <script src="/cvadmin/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
        <script src="/cvadmin/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
        <script src="/cvadmin/vendors/editable-table/mindmup-editabletable.js"></script>
        <script src="/cvadmin/vendors/d3/d3.min.js"></script>
        <script src="/cvadmin/vendors/c3/c3.min.js"></script>
        <script src="/cvadmin/vendors/chartist/dist/chartist.min.js"></script>
        <script src="/cvadmin/vendors/peity/jquery.peity.min.js"></script>
        <script src="/cvadmin/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
        <script src="/cvadmin/vendors/jquery-countTo/jquery.countTo.js"></script>
        <script src="/cvadmin/vendors/nprogress/nprogress.js"></script>
        <script src="/cvadmin/vendors/jquery-steps/build/jquery.steps.min.js"></script>
        <script src="/cvadmin/vendors/chart.js/dist/Chart.bundle.min.js"></script>
        <script src="/cvadmin/vendors/dropify/dist/js/dropify.min.js"></script>
        <script src="/cvadmin/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
        <script src="/cvadmin/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>

        <!-- CLEAN UI ADMIN TEMPLATE MODULES-->
        <!-- v2.0.0 -->
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/core/common/core.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/vendors/common/vendors.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/layouts/common/layouts-pack.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/themes/common/themes.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/menu-left/common/menu-left.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/menu-right/common/menu-right.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/top-bar/common/top-bar.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/footer/common/footer.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/pages/common/pages.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/ecommerce/common/ecommerce.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/cvadmin/modules/apps/common/apps.cleanui.css">
        <script src="/cvadmin/modules/menu-left/common/menu-left.cleanui.js"></script>
        <script src="/cvadmin/modules/menu-right/common/menu-right.cleanui.js"></script>
    </head>
    <body class="cat__config--horizontal cat__menu-left--colorful">
        <div>
            @yield('content')
        </div>

        <!-- Admin Panel Scripts -->
        <script type="text/javascript" src="/js/admin/manifest.js"></script>
        <script type="text/javascript" src="/js/admin/vendor.js"></script>
        <script type="text/javascript" src="/js/admin/main.js"></script>
        <script src="/js/admin/tinymce/jquery.tinymce.min.js"></script>
        <script src="/js/admin/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
          window.TINYMCE = {
                mode: 'none',
                selector: 'textarea',
                height: 150,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true,
                templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                ]
          };
        </script>
    </body>
</html>
