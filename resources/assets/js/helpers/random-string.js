// Generate A Random String of Length = param length
export default function (length) {
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
  var default_length = 8;
  if(typeof length == 'undefined')
    length = default_length;
  var randomstring = '';
  for (var i=0; i<length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum,rnum+1);
  }
  return randomstring;
};

// export default randomString;
