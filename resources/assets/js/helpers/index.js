import randomString from './random-string'
import slugify from './slug'
import {getIndexByID} from './array-functions'

window.randomString = randomString;
window.slugify = slugify;
window.getIndexByID = getIndexByID;
