export default class {
    constructor () {
        this.errors = {};
    }

    hasErrors() {
        return ! _.isEmpty(this.errors);
    }

    has (field) {
        return _.has(this.errors, field);
    }

    all () {
        return this.errors;
    }

    errorMessagesOnly () {
        return _.flatten(_.toArray(this.errors));
    }

    get (field) {
        var value = [];
        if (this.has(field)) {
            value = this.errors[field];
        }
        return value
    }

    isUnKnown() {
        return this.has('unknown')
    }

    set (errors) {
        if (typeof errors === 'object') {
            this.errors = errors;
        } else {
            this.errors = {'unknown': ['Something went wrong. Please try again or contact customer support.']};
        }
    }

    clear (field) {
        if (typeof field === 'undefined') {
            this.errors = {};
        } else {
            Vue.delete(this.errors, field);
        }
    };
}
