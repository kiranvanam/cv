import Vue from 'vue'
import Errors from './errors'

export default class {
    constructor (data) {
        this.setDataWith(data);
        this.errors = new Errors();
        this.busy = false;
        this.successful = false;
        this.ackMsg = '';
    }

    setField(key,value) {
        Vue.set(this, key, value);
    }

    removeField(field) {
        Vue.delete(this, field);
    }

    setDataWith(data) {
        for (let field in data) {
            this.setField(field, data[field]);
        }
    }

    resetData(data) {
        for(let field in this.data()) {
            if(data && data.hasOwnProperty(field) && data[field]){
                this.setField(field, data[field]);
            } else {
                this.removeField(field);
            }
        }
    }

    resetAllToDefaultExcept(data) {
        for(let field in this.data()) {
            if(data[field]){
                this.setField(field, data[field]);
            } else {
                this.setField(field, '');
            }
        }
    }

    isObjectOrArray(field) {
        return Array.isArray(field) || typeof field === 'object';
    }

    data () {
        let data = _.omit(this, ['errors', 'busy', 'ackMsg', 'successful']);
        return data;
    }

    startProcessing () {
        this.errors.clear();
        this.busy = true;
        this.successful = false;
    }


    finishProcessing () {
        this.busy = false;
        this.successful = true;
    }

    isProcessing() {
        return this.busy;
    }

    isSuccess() {
        return this.successful
    }

    success(response) {
        if( (response.status >= 200) && (response.status <= 299))
        {
            this.ackMsg = response.message
            this.finishProcessing();
        } else {
            this.failed(response)
        }
    }

    failed(response) {
        this.successful = false;
        this.set(response);
    }

    clear () {
        this.errors.clear();
        this.busy = false;
        this.successful = false;
    }

    set (response) {
        this.busy = false;
        this.ackMsg =  response.message
        this.errors.set(response.body);
    }

    ack() {
        return this.ackMsg
    }
}
