import config from '../config';

export default class {
    constructor(url = "", baseURI = null) {
        if(baseURI == null) {
            baseURI = config.baseURI;
        }
        this.baseURI = baseURI + url
    }

    resetUrlWith(baseUri) {
        this.baseURI = baseUri;
    }

    appendToUrl(uri) {
        return this.baseURI + uri;
    }

    fetch(uri) {
        uri = this.appendToUrl(uri);
        return this.getRequest('get', uri)
    }

    get(uri, params) {
        uri = this.appendToUrl(uri)
        return this.getRequest('get', uri, params)
    }

    post(uri, form) {
        uri = this.appendToUrl(uri);
        return this.sendForm('post', uri, form);
    }

    postWithoutForm(uri, data) {
        uri = this.appendToUrl(uri);
        return this.postDataWithourForm(uri,data)
    }

    put(uri, form) {
        uri = this.appendToUrl(uri);
        form._method = 'PUT';
        return this.sendForm('post', uri, form);
    }

    patch(uri, form) {
        return this.sendForm('patch', uri, form);
    }

    delete(uri, form=null) {
        uri = this.appendToUrl(uri);
        if(form instanceof Form && Object.keys(form.data()).length)
            return this.deleteRecord(uri, form);
        return this.deleteRecordWihoutForm(uri)
    }

    getRequest(method, uri, params) {
        var header = this.authorizationHeader();
        return new Promise((resolve, reject) => {
            axios[method](uri, { params: params, headers : header })
                .then(response => {
                    resolve(response.data);
                })
                .catch(errors => {
                    reject(errors.data);
                });
        });
    }

    sendForm(method, uri, form) {
        var header = this.authorizationHeader();
        return new Promise((resolve, reject) => {
            form.startProcessing();
            axios[method](uri, form.data(), { headers : header })
                .then(response => {
                    form.success(response.data);
                    resolve(response.data);
                })
                .catch(errors => {
                    form.failed(errors.response.data);
                    reject(errors.data);
                });
        });
    }

    deleteRecord(uri, form) {
        let header = this.authorizationHeader();
        return new Promise((resolve, reject) => {
            form.startProcessing();
            axios.post(uri, {_method: 'DELETE'}, { headers: header })
                .then(response => {
                    //console.log("Deleting Form", response);
                    form.success(response.data);
                    resolve(response.data);
                })
                .catch(errors => {
                    form.failed(errors.response.data);
                    reject(errors.data);
                });
        })
    }

    deleteRecordWihoutForm(uri) {
        let header = this.authorizationHeader();
        return new Promise((resolve, reject) => {
            axios.post(uri, {_method: 'DELETE'}, { headers: header })
                .then(response => {
                    resolve(response.data);
                })
                .catch(errors => {
                    reject(errors.data);
                });
        })
    }

    postDataWithourForm(uri, data) {
        var header = this.authorizationHeader();
        return new Promise((resolve, reject) => {
            axios.post(uri, data, { headers : header })
                .then(response => {
                    resolve(response.data);
                })
                .catch(errors => {
                    reject(errors.data);
                });
        });
    }

    authorizationHeader() {
        var header = {};
        return header;
    }
}
