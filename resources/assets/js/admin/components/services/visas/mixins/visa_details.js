export default {
    mounted: function () {
        this.selectedVisaID = this.$route.params.visaID;
    },
    beforeEnter (to, from, next) {
        if(!this.visas.length) {
            next({name: 'visas'})
        }
    }
}
