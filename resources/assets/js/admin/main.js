
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

window.Vue = require('vue');

import router from './router'
import store from './store'
import Admin from './views/home'
import Form from './core/form'
import spinnerButton from './components/common/spinner-button'
import confirmation from './components/common/confirmation'
import editor from './components/common/tinymce-editor'
import customerSearch from './components/crm/customer-search'
import pulseLoader from 'vue-spinner/src/PulseLoader.vue'
import {Select, Option} from 'element-ui'
import 'element-ui/lib/theme-default/select.css'
import locale from 'element-ui/lib/locale/lang/en';
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'
// Vue.use(ElementUI, { locale });
Vue.use(Select, { locale });
Vue.use(Option, { locale });

window.Form = Form;
Vue.component('spinner-button', spinnerButton);
Vue.component('confirmation', confirmation);
Vue.component('editor', editor);
Vue.component('customer-search', customerSearch);
Vue.component('pulse-loader', pulseLoader);
Vue.component('multiselect', Multiselect);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
    template: '<Admin />',
    components: { Admin }
});
