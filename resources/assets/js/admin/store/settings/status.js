import BaseApi from '../../core/http'

var statusApi = new BaseApi ('/system-settings/status')

export default {
    state: {
        statuses: []
    },
    mutations: {
        SET_STATUSES(state, statuses) {
            state.statuses = statuses;
        },
        ADD_STATUS(state, status) {
            state.statuses.push(status);
        },
        UPDATE_STATUS(state, payload) {
            state.statuses.splice(payload.index, 1, payload.data)
        },
        REMOVE_STATUS(state, index) {
            state.statuses.splice(index, 1);
        }
    },
    actions: {
        setStatuses: ({commit}, statuses) => {
            commit('SET_STATUSES', statuses);
        },
        addStatus: ({state, commit}, status) => {
            statusApi.post('', status)
                            .then((response) => {
                                commit('ADD_STATUS', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a status", errors);
                            });
        },
        updateStatus: ({state, commit}, payload) => {
            var uri = '/' + payload.status.id;
            statusApi.put(uri, payload.status)
                        .then((response) => {
                            commit('UPDATE_STATUS', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating status", errors);
                        });
        },
        removeStatus: ({state, commit}, payload) => {
            var uri = '/' + state.statuses[payload.index].id;
            statusApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_STATUS', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a status", errors);
                            });
        }
    },
    getters: {
        statuses (state) {
            return state.statuses;
        },

        statusesByType: (state) => (type) => {
            if(!type) {
                return [];
            }
            return _.filter(state.statuses, (status) => {
                return status.type == type;
            });
        },

        availabilityStatuses (state) {
            return _.filter(state.statuses, (status) => {
                return status.type == 'availability';
            });
        },

        enquiryStatuses (state) {
            return _.filter(state.statuses, (status) => {
                return status.type == 'enquiry';
            });
        },

        bookingStatuses (state) {
            return _.filter(state.statuses, (status) => {
                return status.type == 'booking';
            });
        },

        statusByID: (state) => (id) => {
            var status = undefined;
            if(id && id > 0) {
                status = _.find(state.statuses, (status) => {
                                return status.id === id;
                            });
            }
            return status;
        },

        statusName: (state, getters) => (id) => {
            var status = getters.statusByID(id);
            return status ? status.name : '';
        }
    }
}
