import BaseApi from '../../core/http'

var adminApi = new BaseApi ('/system-settings/admins')

export default {
    state: {
        admins: [],
    },
    mutations: {
        SET_ADMINS(state, admins) {
            state.admins = admins;
        }
    },
    actions: {
        setAdmins: ({commit}, admins) => {
            commit('SET_ADMINS', admins);
        },
    },
    getters: {
        admins (state) {
            return state.admins;
        },

        adminByID: (state) => (id) => {
            if(!id || id < 0) {
                return undefined;
            }
            return _.find(state.admins, (admin) => {
                return admin.id === id;
            });
        },

        adminName: (state, getters) => (id) => {
            var admin = getters.adminByID(id);
            var name = '';
            if(admin) {
                var last_name = admin.last_name ? admin.last_name : '';
                name = admin.first_name + ' ' + last_name;
            }
            return name;
        },

        getUserFullName: (state, getters) => (user) => {
            if(!user) {
                return ''
            }
            var last_name = user.last_name ? user.last_name : '';
            return user.first_name + ' ' + last_name;
        }
    }
}
