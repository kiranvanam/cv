import BaseApi from '../../core/http'

var serviceApi = new BaseApi ('/system-settings/service')

export default {
    state: {
        services: [],
    },
    mutations: {
        SET_SERVICES(state, services) {
            state.services = services;
        },
        ADD_SERVICE(state, service) {
            state.services.push(service);
        },
        UPDATE_SERVICE(state, payload) {
            state.services.splice(payload.index, 1, payload.data)
        },
        REMOVE_SERVICE(state, index) {
            state.services.splice(index, 1);
        }
    },
    actions: {
        setServices: ({commit}, services) => {
            commit('SET_SERVICES', services);
        },
        addService: ({state, commit}, service) => {
            serviceApi.post('', service)
                            .then((response) => {
                                commit('ADD_SERVICE', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a service", errors);
                            });
        },
        updateService: ({state, commit}, payload) => {
            var uri = '/' + payload.service.id;
            serviceApi.put(uri, payload.service)
                        .then((response) => {
                            commit('UPDATE_SERVICE', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating service", errors);
                        });
        },
        removeService: ({state, commit}, payload) => {
            var uri = '/' + state.services[payload.index].id;
            serviceApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_SERVICE', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a service", errors);
                            });
        }
    },
    getters: {
        services (state) {
            return state.services;
        },

        serviceByID: (state) => (id) => {
            if(!id || id < 0) {
                return undefined;
            }
            return _.find(state.services, (service) => {
                return service.id === id;
            });
        }
    }
}
