import BaseApi from '../../core/http'

var bankApi = new BaseApi ('/system-settings/bank')

export default {
    state: {
        banks: [],
    },
    mutations: {
        SET_BANKS(state, banks) {
            state.banks = banks;
        },
        ADD_BANK(state, bank) {
            state.banks.push(bank);
        },
        UPDATE_BANK(state, payload) {
            state.banks.splice(payload.index, 1, payload.data)
        },
        REMOVE_BANK(state, index) {
            state.banks.splice(index, 1);
        }
    },
    actions: {
        setBanks: ({commit}, banks) => {
            commit('SET_BANKS', banks);
        },
        addBank: ({state, commit}, bank) => {
            bankApi.post('', bank)
                            .then((response) => {
                                commit('ADD_BANK', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a bank", errors);
                            });
        },
        updateBank: ({state, commit}, payload) => {
            var uri = '/' + payload.bank.id;
            bankApi.put(uri, payload.bank)
                        .then((response) => {
                            commit('UPDATE_BANK', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating bank", errors);
                        });
        },
        removeBank: ({state, commit}, payload) => {
            var uri = '/' + state.banks[payload.index].id;
            bankApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_BANK', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a bank", errors);
                            });
        }
    },
    getters: {
        banks (state) {
            return state.banks;
        },

        bankByID: (state) => (id) => {
            if(!id || id < 0) {
                return undefined;
            }
            return _.find(state.banks, (bank) => {
                return bank.id === id;
            });
        }
    }
}
