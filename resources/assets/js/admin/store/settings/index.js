import BaseApi from '../../core/http'
import status from './status'
import service from './service'
import bank from './bank'
import admins from './admins'
import countries from './locations/countries'

var systemSettingsApi = new BaseApi ('/system-settings')

export default {
    actions: {
        fetchSystemSettings: ({state, dispatch, commit}) => {
            systemSettingsApi.fetch('')
                            .then((response) => {
                                dispatch('setServices', response.body.services);
                                dispatch('setStatuses', response.body.statuses);
                                dispatch('setAdmins', response.body.admins);
                                dispatch('setBanks', response.body.banks);
                            })
                            .catch((errors) => {
                                console.log("Error fetching system settings", errors);
                            })
        }
    },
    modules: {
        service,
        bank,
        status,
        admins,
        countries
    }
}
