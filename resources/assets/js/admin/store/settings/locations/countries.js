import BaseApi from '../../../core/http'

var countryApi = new BaseApi('/countries');

export default {
    state: {
        countries: []
    },
    mutations: {
        SET_COUNTRIES(state, countries) {
            state.countries = countries;
        },
        UPDATE_COUNTRY(state, payload) {
            state.countries.splice(payload.index, 1, payload.data)
        }
    },
    actions: {
        fetchCountries: ({commit, dispatch, state}) => {
            countryApi.fetch('/')
                    .then((response) => {
                        commit('SET_COUNTRIES', response.body);
                    })
                    .catch((errors) => {
                        console.log("Error in fetching countries");
                    });
        },
        updateCountry: ({commit, state}, payload) => {
            var uri = '/' + state.countries[payload.country_index].id;
            countryApi.put(uri, payload.data)
                    .then((response) => {
                        commit('UPDATE_COUNTRY', { index: payload.country_index, data: response.body });
                    })
                    .catch((errors) => {
                        console.log("Error updating country details", errors);
                    });
        },
        updateCountryServices: ({commit, state}, payload) => {
            var uri = '/services/' + state.countries[payload.country_index].id;
            countryApi.put(uri, payload.locationServices)
                    .then((response) => {
                        commit('UPDATE_COUNTRY', { index: payload.country_index, data: response.body });
                    })
                    .catch((errors) => {
                        console.log("Error updating country services details", errors);
                    });
        }
    },
    getters: {
        countries(state) {
            return state.countries;
        },

        countryByID: (state) => (id) => {
            return _.find(state.countries, (country) => {
                return country.id === id;
            });
        },

        visaCountries(state, getters) {
            var visaService = _.find(getters.services, (service) => {
                return service.slug === 'visas';
            });
            if(!visaService) {
                return [];
            }
            return _.filter(state.countries, (country) => {
                if(!country.services || !country.services.length) {
                    return false;
                }
                return _.find(country.services, (service) => {
                    return service.id == visaService.id;
                });
            });
        },

        visaCountryName: (state, getters) => (country_id) => {
            var country = getters.countryByID(country_id);
            return country ? country.name : '';
        }
    }
}
