import BaseApi from '../../core/http'

var visaBookingApi = new BaseApi ('/bookings/visa')

export default {
    state: {
        visaBookings: [],
    },
    mutations: {
        SET_VISA_BOOKINGS(state, visaBookings) {
            state.visaBookings = visaBookings;
        },
        ADD_VISA_BOOKING(state, visaBooking) {
            state.visaBookings.push(visaBooking);
        },
        UPDATE_VISA_BOOKING(state, payload) {
            state.visaBookings.splice(payload.index, 1, payload.data)
        },
        REMOVE_VISA_BOOKING(state, index) {
            state.visaBookings.splice(index, 1);
        }
    },
    actions: {
        fetchVisaBookings: ({commit}, search) => {
            visaBookingApi.post('/search', search)
                        .then((response) => {
                            commit('SET_VISA_BOOKINGS', response.body);
                        })
                        .catch((errors) => {
                            console.log("Error in adding fetching visa bookings ", errors);
                        });
        },
        addVisaBooking: ({state, commit}, visaBooking) => {
            visaBookingApi.post('', visaBooking)
                            .then((response) => {
                                // Do this only when the added visa booking meets the current search criteria
                                // commit('ADD_VISA_BOOKING', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a visa booking", errors);
                            });
        },
        updateVisaBooking: ({state, commit}, payload) => {
            var uri = '/' + payload.visaBooking.id;
            visaBookingApi.put(uri, payload.visaBooking)
                        .then((response) => {
                            commit('UPDATE_VISA_BOOKING', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating visa booking", errors);
                        });
        },
        removeVisaBooking: ({state, commit}, payload) => {
            var uri = '/' + state.visaBookings[payload.index].id;
            visaBookingApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_VISA_BOOKING', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a visa booking", errors);
                            });
        }
    },
    getters: {
        visaBookings (state) {
            return state.visaBookings;
        },
        visaBookingByID: (state) => (id) => {
            return _.find(state.visaBookings, (visaBooking) => {
                return visaBooking.id === id;
            });
        }
    }
}
