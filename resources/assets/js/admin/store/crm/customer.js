import BaseApi from '../../core/http'

var customerApi = new BaseApi ('/crm/customer')

export default {
    state: {
        customers: [],
    },
    mutations: {
        CLEAR_CUSTOMER_STATE (state) {
            state.customers = [];
        },
        SET_CUSTOMERS(state, customers) {
            state.customers = customers;
        },
        ADD_CUSTOMER(state, customer) {
            state.customers.push(customer);
        },
        UPDATE_CUSTOMER(state, payload) {
            state.customers.splice(payload.index, 1, payload.data)
        },
        REMOVE_CUSTOMER(state, index) {
            state.customers.splice(index, 1);
        }
    },
    actions: {
        clearCustomerState: ({commit}) => {
            commit('CLEAR_CUSTOMER_STATE');
        },
        searchCustomers: ({commit, dispatch}, search) => {
            dispatch('clearCustomerState');
            customerApi.post('/search', search)
                        .then((response) => {
                            dispatch('setCustomers', response.body);
                        })
                        .catch((errors) => {});
        },
        fetchCustomers: ({commit, dispatch}, search) => {
            return customerApi.post('/fetch', search)
                        .then((response) => {
                            return response.body;
                        });
        },
        setCustomers: ({commit}, customers) => {
            commit('SET_CUSTOMERS', customers);
        },
        addCustomer: ({state, commit}, customer) => {
            customerApi.post('', customer)
                            .then((response) => {
                                commit('ADD_CUSTOMER', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a customer", errors);
                            });
        },
        updateCustomer: ({state, commit}, payload) => {
            var uri = '/' + payload.customer.id;
            customerApi.put(uri, payload.customer)
                        .then((response) => {
                            commit('UPDATE_CUSTOMER', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating customer", errors);
                        });
        },
        removeCustomer: ({state, commit}, payload) => {
            var uri = '/' + state.customers[payload.index].id;
            customerApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_CUSTOMER', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a customer", errors);
                            });
        }
    },
    getters: {
        customers (state) {
            return state.customers;
        },

        customerByID: (state) => (id) => {
            return _.find(state.customers, (customer) => {
                return customer.id === id;
            });
        }
    }
}
