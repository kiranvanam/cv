import customer from './customer'

export default {
    modules: {
        customer
    }
}
