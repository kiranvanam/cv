import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import services from './services'
import settings from './settings'
import crm from './crm'
import enquiries from './enquiries'
import bookings from './bookings'

var modules = {
    services,
    settings,
    crm,
    enquiries,
    bookings
};

export default new Vuex.Store({
    strict: true,
    modules
});
