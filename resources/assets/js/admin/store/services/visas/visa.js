import BaseApi from '../../../core/http'

var visaApi = new BaseApi ('/visas')
var visaPricingApi = new BaseApi ('/visas/pricing')

/*function getIndexByID(visas, id) {
    return _.findIndex(visas, (visa) => {
        return visa.id === id;
    });
}*/
export default {
    state: {
        visas: [],
    },
    mutations: {
        CLEAR_VISA_STATE(state) {
            state.visas = [];
        },
        SET_VISAS(state, visas) {
            state.visas = visas;
        },
        ADD_VISA(state, visa) {
            state.visas.push(visa);
        },
        UPDATE_VISA(state, payload) {
            state.visas.splice(payload.index, 1, payload.data)
        },
        REMOVE_VISA(state, index) {
            state.visas.splice(index, 1);
        },
        ADD_VISA_PRICING (state, payload) {
            state.visas[payload.visaIndex].prices.push(payload.data);
        },
        UPDATE_VISA_PRICING(state, payload) {
            state.visas[payload.visaIndex].prices.splice(payload.visaPriceIndex, 1, payload.data);
        }
    },
    actions: {
        clearVisaState: ({commit, state}) => {
            commit('CLEAR_VISA_STATE');
        },
        fetchVisas: ({commit, state}, form) => {
            visaApi.fetch ('')
                    .then((response) => {
                        commit('SET_VISAS', response.body);
                    })
                    .catch((errors) => {
                        console.log("Error Fetching Visas ", errors);
                    });
        },
        setVisas: ({commit}, visas) => {
            commit('SET_VISAS', visas);
        },
        addVisa: ({state, commit}, visa) => {
            visaApi.post('', visa)
                            .then((response) => {
                                commit('ADD_VISA', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a visa", errors);
                            });
        },
        updateVisa: ({state, commit}, visa) => {
            var uri = '/' + visa.id;
            var index = getIndexByID(state.visas, visa.id);
            visaApi.put(uri, visa)
                        .then((response) => {
                            commit('UPDATE_VISA', { index: index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating visa", errors);
                        });
        },
        removeVisa: ({state, commit}, payload) => {
            var uri = '/' + payload.visa.id;
            var index = getIndexByID(state.visas, payload.visa.id);
            visaApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_VISA', index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a visa", errors);
                            });
        },
        addVisaPricing: ({state, commit}, visaPricing) => {
            var visaIndex = getIndexByID(state.visas, visaPricing.visa_id);
            console.log("Visa Index ", visaIndex);
            visaPricingApi.post('', visaPricing)
                            .then((response) => {
                                commit('ADD_VISA_PRICING', { visaIndex: visaIndex, data: response.body});
                            });
        },
        updateVisaPricing: ({state, commit}, payload) => {
            // Payload: visaPricing, visaPriceIndex
            var uri = '/' + payload.visaPricing.id;
            var visaIndex = getIndexByID(state.visas, payload.visaPricing.visa_id);
            visaPricingApi.put(uri, payload.visaPricing)
                            .then((response) => {
                                commit('UPDATE_VISA_PRICING', { visaIndex: visaIndex, visaPriceIndex: payload.visaPriceIndex, data: response.body});
                                console.log("Update Visa Pricing", response.body);
                            });
        },
        removeVisaPricing: ({state, commit}, payload) => {
            // Payload: visaPriceIndex, visa_id
            var visaIndex = getIndexByID(state.visas, payload.visa_id);
            var visaPricingID = state.visas[visaIndex].prices[visaPriceIndex]
            visaPricingApi.delete()
        }
    },
    getters: {
        visas (state) {
            return state.visas;
        },

        visasByCountryID: (state) => (country_id) => {
            if(!country_id) {
                return [];
            }
            return _.filter(state.visas, (visa) => {
                return visa.country_id == country_id;
            })
        },

        visaByID: (state) => (id) => {
            if(!id || id < 0) {
                return undefined;
            }
            return _.find(state.visas, (visa) => {
                return visa.id === id;
            });
        },

        visaName: (state, getters) => (id) => {
            var visa = getters.visaByID (id);
            var name = '';
            if(visa) {
                name = visa.name;
            }
            return name;
        },

        getVisaPricing: (state, getters) => (visa_pricing_id, visa) => {
            if(!visa_pricing_id || !visa) {
                return {};
            }
            return _.find(visa.prices, (price) => {
                return price.id === visa_pricing_id;
            });
        }
    }
}
