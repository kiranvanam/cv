import BaseApi from '../../../core/http'

var visaDocumentFieldApi = new BaseApi ('/visas/settings/visa-document-field')

export default {
    state: {
        visaDocumentFields: [],
    },
    mutations: {
        SET_VISA_DOCUMENT_FIELDS(state, visaDocumentFields) {
            state.visaDocumentFields = visaDocumentFields;
        },
        ADD_VISA_DOCUMENT_FIELD(state, visaDocumentField) {
            state.visaDocumentFields.push(visaDocumentField);
        },
        UPDATE_VISA_DOCUMENT_FIELD(state, payload) {
            state.visaDocumentFields.splice(payload.index, 1, payload.data)
        },
        REMOVE_VISA_DOCUMENT_FIELD(state, index) {
            state.visaDocumentFields.splice(index, 1);
        }
    },
    actions: {
        setVisaDocumentFields: ({commit}, visaDocumentFields) => {
            commit('SET_VISA_DOCUMENT_FIELDS', visaDocumentFields);
        },
        addVisaDocumentField: ({state, commit}, visaDocumentField) => {
            visaDocumentFieldApi.post('', visaDocumentField)
                            .then((response) => {
                                commit('ADD_VISA_DOCUMENT_FIELD', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a visa document field", errors);
                            });
        },
        updateVisaDocumentField: ({state, commit}, payload) => {
            var uri = '/' + payload.visaDocumentField.id;
            visaDocumentFieldApi.put(uri, payload.visaDocumentField)
                        .then((response) => {
                            commit('UPDATE_VISA_DOCUMENT_FIELD', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating visa document field", errors);
                        });
        },
        removeVisaDocumentField: ({state, commit}, payload) => {
            var uri = '/' + state.visaDocumentFields[payload.index].id;
            visaDocumentFieldApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_VISA_DOCUMENT_FIELD', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a visa document field", errors);
                            });
        }
    },
    getters: {
        visaDocumentFields (state) {
            return state.visaDocumentFields;
        },

        visaDocumentFieldByID: (state) => (id) => {
            return _.find(state.visaDocumentFields, (visaDocumentField) => {
                return visaDocumentField.id === id;
            });
        }
    }
}
