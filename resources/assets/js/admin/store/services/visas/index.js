import BaseApi from '../../../core/http'
import visaGeneralTerms from './visa-general-terms-conditions'
import visaType from './visa-type'
import visaDocumentField from './visa-document-field'
import visa from './visa'

var visaSettingsApi = new BaseApi ('/visas/settings')

export default {
    actions: {
        fetchVisaSettings: ({state, dispatch, commit}) => {
            visaSettingsApi.fetch('')
                            .then ((response) => {
                                dispatch('setVisaTypes', response.body.visaTypes);
                                dispatch('setVisaGeneralTerms', response.body.visaGeneralTerms);
                                dispatch('setVisaDocumentFields', response.body.visaDocumentFields);
                            })
                            .catch((errors) => {
                                console.log("Error In Fetching Visa Settings", errors)
                            })
        }
    },
    modules: {
        visaGeneralTerms,
        visaType,
        visaDocumentField,
        visa
    }
}
