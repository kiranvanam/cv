import BaseApi from '../../../core/http'

var visaTypeApi = new BaseApi ('/visas/settings/visa-type')

export default {
    state: {
        visaTypes: [],
    },
    mutations: {
        SET_VISA_TYPES(state, visaTypes) {
            state.visaTypes = visaTypes;
        },
        ADD_VISA_TYPE(state, visaType) {
            state.visaTypes.push(visaType);
        },
        UPDATE_VISA_TYPE(state, payload) {
            state.visaTypes.splice(payload.index, 1, payload.data)
        },
        REMOVE_VISA_TYPE(state, index) {
            state.visaTypes.splice(index, 1);
        }
    },
    actions: {
        setVisaTypes: ({commit}, visaTypes) => {
            commit('SET_VISA_TYPES', visaTypes);
        },
        addVisaType: ({state, commit}, visaType) => {
            visaTypeApi.post('', visaType)
                            .then((response) => {
                                commit('ADD_VISA_TYPE', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a visa type", errors);
                            });
        },
        updateVisaType: ({state, commit}, payload) => {
            var uri = '/' + payload.visaType.id;
            visaTypeApi.put(uri, payload.visaType)
                        .then((response) => {
                            commit('UPDATE_VISA_TYPE', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating visa type", errors);
                        });
        },
        removeVisaType: ({state, commit}, payload) => {
            var uri = '/' + state.visaTypes[payload.index].id;
            visaTypeApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_VISA_TYPE', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a visa type", errors);
                            });
        }
    },
    getters: {
        visaTypes (state) {
            return state.visaTypes;
        },

        visaTypeByID: (state) => (id) => {
            return _.find(state.visaTypes, (visaType) => {
                return visaType.id === id;
            });
        }
    }
}
