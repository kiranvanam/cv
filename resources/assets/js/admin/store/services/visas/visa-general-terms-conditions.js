import BaseApi from '../../../core/http'

var visaTermsApi = new BaseApi ('/visas/settings/visa-general-terms-conditions')

export default {
    state: {
        generalTermsConditions: {}
    },
    mutations: {
        SET_VISA_GENERAL_TERMS_CONDITIONS(state, generalTermsConditions) {
            state.generalTermsConditions = generalTermsConditions
        },
    },
    actions: {
        setVisaGeneralTerms: ({commit}, terms) => {
            var data = {};
            if(terms && terms.length) {
                data = terms[0];
            }
            commit('SET_VISA_GENERAL_TERMS_CONDITIONS', data);
        },
        updateVisaGeneralTerms: ({state, commit}, visaTermsConditions) => {
            visaTermsApi.post('', visaTermsConditions)
                        .then((response) => {
                            console.log("Update Visa ")
                            commit('SET_VISA_GENERAL_TERMS_CONDITIONS', response.body);
                        })
                        .catch((errors) => {
                            console.log("Error updating visa type", errors);
                        });
        },

    },
    getters: {
        generalTermsConditions (state) {
            return state.generalTermsConditions;
        }
    }
}
