import visaEnquiry from './visa-enquiry'

export default {
    modules: {
        visaEnquiry
    }
}
