import BaseApi from '../../core/http'

var visaEnquiryApi = new BaseApi ('/enquiries/visa')

export default {
    state: {
        visaEnquiries: [],
    },
    mutations: {
        SET_VISA_ENQUIRIES(state, visaEnquiries) {
            state.visaEnquiries = visaEnquiries;
        },
        ADD_VISA_ENQUIRY(state, visaEnquiry) {
            state.visaEnquiries.push(visaEnquiry);
        },
        UPDATE_VISA_ENQUIRY(state, payload) {
            state.visaEnquiries.splice(payload.index, 1, payload.data)
        },
        REMOVE_VISA_ENQUIRY(state, index) {
            state.visaEnquiries.splice(index, 1);
        }
    },
    actions: {
        fetchVisaEnquiries: ({commit}, search) => {
            visaEnquiryApi.post('/search', search)
                        .then((response) => {
                            commit('SET_VISA_ENQUIRIES', response.body);
                        })
                        .catch((errors) => {
                            console.log("Error in adding fetching visa enquiries ", errors);
                        });
        },
        addVisaEnquiry: ({state, commit}, visaEnquiry) => {
            visaEnquiryApi.post('', visaEnquiry)
                            .then((response) => {
                                // Do this only when the added visa enquiry meets the current search criteria
                                // commit('ADD_VISA_ENQUIRY', response.body);
                            })
                            .catch((errors) => {
                                console.log("Error adding a visa enquiry", errors);
                            });
        },
        updateVisaEnquiry: ({state, commit}, payload) => {
            var uri = '/' + payload.visaEnquiry.id;
            visaEnquiryApi.put(uri, payload.visaEnquiry)
                        .then((response) => {
                            commit('UPDATE_VISA_ENQUIRY', { index: payload.index, data: response.body });
                        })
                        .catch((errors) => {
                            console.log("Error updating visa enquiry", errors);
                        });
        },
        removeVisaEnquiry: ({state, commit}, payload) => {
            var uri = '/' + state.visaEnquiries[payload.index].id;
            visaEnquiryApi.delete(uri, payload.form)
                            .then((response) => {
                                commit('REMOVE_VISA_ENQUIRY', payload.index);
                            })
                            .catch((errors) => {
                                console.log("Error removing a visa enquiry", errors);
                            });
        },
        bookVisaEnquiry: ({state, commit}, payload) => {
            // Payload: enquiry => visa enquiry, index => visa enquiry index, form => form
            var uri = '/book/' + payload.enquiry.id;
            visaEnquiryApi.post(uri, payload.form)
                            .then((response) => {
                                commit('UPDATE_VISA_ENQUIRY', {index: payload.index, data: response.body});
                            })
                            .catch((errors) => {
                                console.log("Error removing a visa enquiry", errors);
                            });
        }
    },
    getters: {
        visaEnquiries (state) {
            return state.visaEnquiries;
        },
        visaEnquiryByID: (state) => (id) => {
            return _.find(state.visaEnquiries, (visaEnquiry) => {
                return visaEnquiry.id === id;
            });
        }
    }
}
