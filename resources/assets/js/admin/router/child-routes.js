import services from './services'
import settings from './settings'
import crm from './crm'
import enquiries from './enquiries'
import bookings from './bookings'

var modules = [
    services,
    settings,
    crm,
    enquiries,
    bookings,
];

var componentRoutes = [];

modules.forEach( (module) => {
    componentRoutes = componentRoutes.concat(module);
} );

componentRoutes.push({
    path: '',
    component: require('../components/dashboard')
});

export default componentRoutes;
