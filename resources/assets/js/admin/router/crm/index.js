import crm from './crm'

export default [
    {
        path: 'crm',
        name: 'crm',
        component: require('../../components/crm/home'),
        children: crm
    }
]
