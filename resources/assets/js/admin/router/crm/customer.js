export default [
    {
        path: 'customer',
        name: 'customer',
        component: require('../../components/crm/customer'),
        meta: { title: 'Customers' }
    }
]
