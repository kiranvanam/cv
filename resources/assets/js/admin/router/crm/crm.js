import customer from './customer'

var modules = [
    customer
];
var customerRoutes = [];
modules.forEach((module) => {
    customerRoutes = customerRoutes.concat(module);
});

export default customerRoutes;
