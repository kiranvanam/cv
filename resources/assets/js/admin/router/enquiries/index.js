import enquiries from './enquiries'

export default [
    {
        path: 'enquiries',
        name: 'enquiries',
        component: require('../../components/enquiries/home'),
        children: enquiries
    }
]
