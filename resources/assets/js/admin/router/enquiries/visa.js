var childRoutes = [
    {
        path: 'add',
        name: 'add-visa-enquiry',
        component: require('../../components/enquiries/visa/add-update'),
        meta: { title: 'Add' }
    },
    {
        path: 'update/:visaEnquiryIndex',
        name: 'update-visa-enquiry',
        component: require('../../components/enquiries/visa/add-update'),
        props: true,
        meta: { title: 'Update', doNotShowInForLoop: true }
    },
    {
        path: 'list',
        name: 'list-visa-enquiries',
        component: require('../../components/enquiries/visa/list'),
        meta: { title: 'List' }
    }
];

var visaRoute = {
    path: 'visa',
    name: 'visa-enquiry-home',
    component: require('../../components/enquiries/visa/home'),
    meta: { title: 'Visa Enquiry'},
    children: childRoutes
}

export default visaRoute;
