import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import childRoutes from './child-routes'

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/admin',
            component: require('../views/holder.vue'),
            children: childRoutes
        }
    ]
});

router.beforeEach((to, from, next) => {
    next();
});

export default router;
