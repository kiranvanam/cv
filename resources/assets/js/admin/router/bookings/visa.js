var childRoutes = [
    {
        path: 'add',
        name: 'add-visa-booking',
        component: require('../../components/bookings/visa/add-update'),
        meta: { title: 'Add' },
        beforeRouteEnter (to, from, next) {
          next(vm => {
            console.log("VM ", vm);
          })
        }
    },
    {
        path: 'update/:visaBookingIndex',
        name: 'update-visa-booking',
        component: require('../../components/bookings/visa/add-update'),
        props: true,
        meta: { title: 'Update', doNotShowInForLoop: true }
    },
    {
        path: 'list',
        name: 'list-visa-bookings',
        component: require('../../components/bookings/visa/list'),
        meta: { title: 'List' }
    }
];

var visaRoute = {
    path: 'visa',
    name: 'visa-booking-home',
    component: require('../../components/bookings/visa/home'),
    meta: { title: 'Visa Booking'},
    children: childRoutes
}

export default visaRoute;
