import bookings from './bookings'

export default [
    {
        path: 'bookings',
        name: 'bookings',
        component: require('../../components/bookings/home'),
        children: bookings
    }
]
