export default [
    {
        path: 'status',
        name: 'system-settings-status',
        component: require('../../components/settings/status'),
        meta: { title: 'Status' }
    }
]
