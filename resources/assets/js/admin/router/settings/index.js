import settings from './settings'

export default [
    {
        path: 'system-settings',
        name: 'system-settings',
        component: require('../../components/settings/home'),
        children: settings
    }
]
