var locationChildRoutes = [
    {
        path: 'countries',
        name: 'countries',
        component: require('../../components/settings/locations/countries'),
        meta: { title: 'Countries' }
    },
    {
        path: 'destinations',
        name: 'destinations',
        component: require('../../components/settings/locations/destinations'),
        meta: { title: 'Destinations' }
    }
];

export default [
    {
        path: 'locations',
        name: 'locations',
        component: require('../../components/settings/locations/home'),
        children: locationChildRoutes,
        meta: {title: 'Locations'}
    }
]