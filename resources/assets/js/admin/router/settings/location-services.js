var locationServicesChildRoutes = [
    {
        path: 'country-services',
        name: 'country-services',
        component: require('../../components/settings/location-services/countries'),
        meta: { title: 'Countries' }
    },
    {
        path: 'destination-services',
        name: 'destination-services',
        component: require('../../components/settings/location-services/destinations'),
        meta: { title: 'Destinations' }
    }
];

export default [
    {
        path: 'location-services',
        name: 'location-services',
        component: require('../../components/settings/location-services/home'),
        children: locationServicesChildRoutes,
        meta: {title: 'Location Services'}
    }
]
