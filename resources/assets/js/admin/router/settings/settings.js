import bank from './bank'
import status from './status'
import service from './service'
import locations from './locations'
import locationServices from './location-services'

var modules = [
    status,
    service,
    locations,
    locationServices,
    bank,
];
var settingRoutes = [];
modules.forEach((module) => {
    settingRoutes = settingRoutes.concat(module);
});

export default settingRoutes;
