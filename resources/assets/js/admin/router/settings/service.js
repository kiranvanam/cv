export default [
    {
        path: 'service',
        name: 'system-settings-service',
        component: require('../../components/settings/service'),
        meta: { title: 'Services' }
    }
];
