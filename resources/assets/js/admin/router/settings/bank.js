export default [
    {
        path: 'bank',
        name: 'system-settings-bank',
        component: require('../../components/settings/bank'),
        meta: { title: 'Banks' }
    }
]
