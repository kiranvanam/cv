import visaSettingRoutes from './setting-routes'
import visaDetailRoutes from './visa-detail-routes'
import store from '../../../store'

var visaRoutes = [
    {
        path: 'settings',
        name: 'visas-settings',
        component: require('../../../components/services/visas/settings/home'),
        children: visaSettingRoutes
    },
    {
        path: '',
        name: 'visas',
        component: require('../../../components/services/visas/visas')
    },
    {
        path: 'details/:visaID',
        name: 'visas-detail',
        component: require('../../../components/services/visas/details/home'),
        children: visaDetailRoutes
    }
];

export default [
    {
        path: 'visas',
        component: require('../../../components/services/visas/home'),
        children: visaRoutes
    }
]
