export default [
    {
        path: 'pricing',
        name: 'visa-pricing',
        component: require('../../../components/services/visas/details/pricing'),
        meta: { title: 'Pricing' }
    },
    {
        path: 'general-details',
        name: 'visa-specific-details',
        component: require('../../../components/services/visas/details/general-details'),
        meta: { title: 'General Details' }
    },
    {
        path: 'required-documents',
        name: 'visa-required-documents',
        component: require('../../../components/services/visas/details/required-documents'),
        meta: { title: 'Required Documents' }
    }
]
