export default [
    {
        path: 'visa-types',
        name: 'visa-types',
        component: require('../../../components/services/visas/settings/visa-types'),
        meta: { title: 'Visa Types' }
    },
    {
        path: 'visa-document-fields',
        name: 'visa-document-fields',
        component: require('../../../components/services/visas/settings/visa-document-fields'),
        meta: { title: 'Required Document Fields' }
    },
    {
        path: 'visa-general-terms-conditions',
        name: 'visa-general-terms-conditions',
        component: require('../../../components/services/visas/settings/visa-general-terms-conditions'),
        meta: { title: 'Terms & Conditions' }
    }
];
