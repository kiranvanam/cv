import visas from './visas'

var serviceRouteModules = [
    visas
];

var serviceRoutes = [];

serviceRouteModules.forEach( (module) => {
     serviceRoutes = serviceRoutes.concat(module);
} );

export default serviceRoutes;
